package com.kolibru.veloportation;

import android.location.Location;

import com.kolibru.veloportation.models.PlacePosition;

/**
 * Created by Andrej_Maligin on 31.10.2016.
 */

public class FinanceClass {

    public static long foundPrice(PlacePosition placePosition_from, PlacePosition placePosition_to){
        double price=0;
        try {
            if (placePosition_from != null && placePosition_to != null) {
                price=getDistance(placePosition_from,placePosition_to)*23*0.001;
                price+=50;
                price=price*1.5f;
            }
        }
        catch (Exception e){
            price=0;
        }
        return Math.round(price);
    }

    public static float getDistance(double from_lat,double from_lon,double to_lat,double to_lon) {
        try {
            float[] results = new float[1];
            Location.distanceBetween(from_lat, from_lon,
                    to_lat, to_lon, results);
            return results[0];
        }
        catch (Exception e){
            return 0;
        }
    }

    public static int getDistance(PlacePosition placePosition_from,PlacePosition placePosition_to) {
        if(placePosition_from!=null && placePosition_to!=null) {
            return Math.round(getDistance(
                    placePosition_from.getLatitude(),
                    placePosition_from.getLongitude(),
                    placePosition_to.getLatitude(),
                    placePosition_to.getLongitude()
            ));
        }
        else {
            return 0;
        }
    }
}
