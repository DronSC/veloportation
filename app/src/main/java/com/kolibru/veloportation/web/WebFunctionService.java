package com.kolibru.veloportation.web;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.OnlineUsers;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.models.OrderPosition;
import com.kolibru.veloportation.models.PlacePosition;
import com.kolibru.veloportation.models.TakeOrder;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.models.UserPosition;
import com.squareup.okhttp.OkHttpClient;


import java.io.File;

import io.realm.Realm;
import io.realm.RealmObject;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedFile;


public class WebFunctionService  {

    public static String BASE_URL = "http://prokatvros.temp.swtest.ru/veloportation/api/index.php";

    private static String TOKEN = null;

    private static WebFunctionService instance;
    private Context context;

    public static WebFunctionService getInstance(Context context) {
        if (null == instance){
            instance = new WebFunctionService();
            instance.context = context;
        }
        return instance;
    }

    Gson gson = new GsonBuilder()
            .setExclusionStrategies(getExclusionStrategy())
            .create();



    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Accept", "application/json");
            request.addHeader("Accept-Content", "application/json");
            request.addHeader("Cookie", getTOKEN());

        }
    };
    private RestAdapter mRest = new RestAdapter.Builder()
            .setEndpoint(BASE_URL)
            .setConverter(new GsonConverter(gson))
            .setClient(new OkClient(new OkHttpClient()))
            .setRequestInterceptor(requestInterceptor)
            .build();

    private WebService mWebService = mRest.create(WebService.class);

    public static String getTOKEN() {
        if(TOKEN!=null)
            return "veloport="+TOKEN;
        else
            return "";
    }

    public static String getTOKEN_orig() {
        return TOKEN;
    }

    public static void setTOKEN(String TOKEN) {
        WebFunctionService.TOKEN=TOKEN;
    }

    public void registration(String login,String pass,String surname,String name,String patro,String mobile,int type, Callback<BaseStruct<User>> cb) {
         try {
             mWebService.registration(login,pass,surname,name,patro,mobile,type,"android",cb);
         } catch (Exception e) {
             Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
         }
    }

    public void updateFio(String surname,String name,String patro, Callback<BaseStruct<User>> cb) {
        try {
            mWebService.updateFio(surname,name,patro,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void updatePass(String pass, Callback<BaseStruct<User>> cb) {
        try {
            mWebService.updatePass(pass,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void updateContact(String mobile, Callback<BaseStruct<User>> cb) {
        try {
            mWebService.updateContact(mobile,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void auth(String login,String pass, Callback<BaseStruct<User>> cb) {
        try {
            mWebService.auth(login,pass,"android",cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void logout(Callback<BaseStruct<User>> cb) {
        try {
            mWebService.logout("android",cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void addOrder(String phone, String message, String cost, PlacePosition addres_from, PlacePosition addres_to, String wait_time, Callback<BaseStruct<Order>> cb) {
        try {
            mWebService.addOrder(
                    phone,
                    addres_from.getName(),addres_from.getLatitude(),addres_from.getLongitude(),
                    addres_to.getName(),addres_to.getLatitude(),addres_to.getLongitude(),
                    message,
                    cost,
                    wait_time,
                    cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void addOpinion(long id_user, String message, String rating, Callback<BaseStruct<User>> cb) {
        try {
            mWebService.opinion(
                    id_user,
                    message,
                    rating,
                    cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void getOnlineUsers(Callback<BaseStruct<OnlineUsers>> cb) {
        try {
            mWebService.getOnlineUser("android",cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void saveOrderPosition(double lat,double lon, Callback<BaseStruct<UserPosition>> cb) {
        try {
            mWebService.saveOrderPosition(lat,lon,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void getOrders(Callback<BaseStruct<Order>> cb) {
        try {
            mWebService.getOrders(cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void cancelOrder(long id_order,Callback<BaseStruct<Order>> cb) {
        try {
            mWebService.cancelOrder(id_order,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void getOrder(long id,Callback<BaseStruct<Order>> cb) {
        try {
            mWebService.getOrder(id,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void uploadAva(String path,final Callback<BaseStruct<User>> cb) {
        final TypedFile typedFile = new TypedFile("multipart/form-data", new File(path));
        try {
            mWebService.uploadAva(typedFile,cb);
        } catch (Exception e) {

        }
    }

    public void getOrdersPosition(Callback<BaseStruct<OrderPosition>> cb) {
        try {
            mWebService.getOrdersPosition("empty",cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void getOrdersPosition(long id_order,Callback<BaseStruct<OrderPosition>> cb) {
        try {
            mWebService.getOrdersPosition(id_order,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }


    public void takeOrder(long id_order,String message,String end_time,int can_see,Callback<BaseStruct<Order>> cb) {
        try {
            mWebService.takeOrder(id_order,message,end_time,can_see,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    public void confirmOrder(long id_order,Callback<BaseStruct<Order>> cb) {
        try {
            mWebService.confirmOrder(id_order,cb);
        } catch (Exception e) {
            Log.e("getRoutes","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
    }

    private ExclusionStrategy getExclusionStrategy() {
        return new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        };
    }

    /**
     * Активен интернет?
     * @return
     */
    public static boolean isInternetOn(Context mContext) {
        if(mContext!=null) {
            ConnectivityManager connec = (ConnectivityManager) mContext.getApplicationContext()
                    .getSystemService(mContext.getApplicationContext().CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connec.getActiveNetworkInfo();
            return (networkInfo != null && networkInfo.isConnected());
        }
        return true;
    }


}