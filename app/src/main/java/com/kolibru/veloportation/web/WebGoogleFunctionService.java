package com.kolibru.veloportation.web;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kolibru.veloportation.App;
import com.kolibru.veloportation.models.StructGooglePosition;
import com.squareup.okhttp.OkHttpClient;

import io.realm.RealmObject;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by artoymtkachenko on 17.07.15.
 */
public class WebGoogleFunctionService {

    public static String TYPE_STREET = "street_address";
    public static String TYPE_CITY = "administrative_area_level_2";
    public static String TYPE_LOCAL = "locality";

    public static String LANGUAGE_RU = "ru";
    public static String LANGUAGE_EN = "en";

    public static String BASE_URL = "https://maps.googleapis.com/maps/api";

    Gson gson = new GsonBuilder()
            .setExclusionStrategies(getExclusionStrategy())
            .create();

    private RestAdapter mRest = new RestAdapter.Builder()
            .setEndpoint(BASE_URL)
            .setConverter(new GsonConverter(gson))
            .setClient(new OkClient(new OkHttpClient()))
            .build();

    private WebGoogleService mWebService = mRest.create(WebGoogleService.class);

    public void getLocationByLatLAng(Double lat,Double lon, Callback<StructGooglePosition> cb){
        try {
            mWebService.getLocationByLatLAng(Double.toString(lat)+","+Double.toString(lon),TYPE_STREET,LANGUAGE_RU, App.API_GOOGLE,cb);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getLocationByAddress(String str, Callback<StructGooglePosition> cb){
        try {
            mWebService.getLocationByAddress(str,TYPE_STREET,LANGUAGE_RU, App.API_GOOGLE,cb);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ExclusionStrategy getExclusionStrategy() {
        return new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        };
    }

}