package com.kolibru.veloportation.web;


import com.kolibru.veloportation.models.StructGooglePosition;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Ссылки на функции API
 */
public interface WebGoogleService {
    
    @GET("/geocode/json")
    void getLocationByLatLAng(
            @Query("latlng") String latlng,
            @Query("result_type") String result_type,
            @Query("language") String language,
            @Query("key") String key,
            Callback<StructGooglePosition> cb
    );

    @GET("/geocode/json")
    void getLocationByAddress(
            @Query("address") String address,
            @Query("result_type") String result_type,
            @Query("language") String language,
            @Query("key") String key,
            Callback<StructGooglePosition> cb
    );
}
