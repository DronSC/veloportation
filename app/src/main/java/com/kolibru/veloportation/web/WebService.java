package com.kolibru.veloportation.web;

import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.OnlineUsers;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.models.OrderPosition;
import com.kolibru.veloportation.models.TakeOrder;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.models.UserPosition;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

/**
 * Ссылки на функции API
 */
public interface WebService {


    /**
     * все пользователи - для отладки
     * @return
     */
    @GET("/c_user/all")
    void getProfile(
            Callback<BaseStruct<User>> cb
    );

    /**
     * Регистрация
     * @return
     */
    @FormUrlEncoded
    @POST("/c_user/registration")
    void registration(
            @Field("login") String login,
            @Field("pass") String pass,
            @Field("surname") String surname,
            @Field("name") String name,
            @Field("patro") String patro,
            @Field("mobile") String mobile,
            @Field("type") int type,
            @Field("platform") String platform,
            Callback<BaseStruct<User>> cb
    );

    /**
     * Изменить профиль
     * В частности для фио
     * @param surname
     * @param name
     * @param patro
     * @param cb
     */
    @FormUrlEncoded
    @POST("/c_user/update")
    void updateFio(
            @Field("surname") String surname,
            @Field("name") String name,
            @Field("patro") String patro,
            Callback<BaseStruct<User>> cb
    );

    /**
     * Изменить профиль
     * В частности для телефона
     * @param mobile
     * @param cb
     */
    @FormUrlEncoded
    @POST("/c_user/update")
    void updateContact(
            @Field("mobile") String mobile,
            Callback<BaseStruct<User>> cb
    );

    /**
     * Изменить профиль
     * В частности для пароля
     * @param pass
     * @param cb
     */
    @FormUrlEncoded
    @POST("/c_user/update")
    void updatePass(
            @Field("pass") String pass,
            Callback<BaseStruct<User>> cb
    );


    /**
     * Изменить аву
     * @param cb
     */
    @Multipart
    @POST("/c_user/update_ava")
    void uploadAva(
            @Part("file") TypedFile file,
            Callback<BaseStruct<User>> cb
    );



    /**
     * Авторизация
     * @return
     */
    @FormUrlEncoded
    @POST("/c_user/auth")
    void auth(
            @Field("login") String login,
            @Field("pass") String pass,
            @Field("platform") String surname,
            Callback<BaseStruct<User>> cb
    );

    /**
     * Выход из учетки
     * @param surname
     * @param cb
     */
    @FormUrlEncoded
    @POST("/c_user/logout")
    void logout(
            @Field("platform") String surname,
            Callback<BaseStruct<User>> cb
    );

    /**
     * Создание заказа
     * @param phone
     * @param address_sender
     * @param address_sender_lat
     * @param address_sender_lon
     * @param address_delivery
     * @param address_delivery_lat
     * @param address_delivery_lon
     * @param message
     * @param cost
     * @param cb
     */
    @FormUrlEncoded
    @POST("/c_order/save")
    void addOrder(
            @Field("phone") String phone,
            @Field("address_from") String address_sender,
            @Field("address_from_lat") double address_sender_lat,
            @Field("address_from_lon") double address_sender_lon,
            @Field("address_to") String address_delivery,
            @Field("address_to_lat") double address_delivery_lat,
            @Field("address_to_lon") double address_delivery_lon,
            @Field("message") String message,
            @Field("cost") String cost,
            @Field("wait_time") String wait_time,
            Callback<BaseStruct<Order>> cb
    );

    /**
     * Принятие заказа
     * @param id_order
     * @param end_time
     * @param message
     * @param can_see
     * @param cb
     */
    @FormUrlEncoded
    @POST("/c_order/i_do")
    void takeOrder(
            @Field("id_order") long id_order,
            @Field("message") String end_time,
            @Field("end_time") String message,
            @Field("can_see") int can_see,
            Callback<BaseStruct<Order>> cb
    );

    @FormUrlEncoded
    @POST("/c_user/getOnlineUser")
    void getOnlineUser(
            @Field("platform") String platform,
            Callback<BaseStruct<OnlineUsers>> cb
    );


    @FormUrlEncoded
    @POST("/c_user/opinion")
    void opinion(
            @Field("id_user") long id_user,
            @Field("message") String message,
            @Field("major_rating") String rating,
            Callback<BaseStruct<User>> cb
    );

    /**
     * Отмена заказа - тестирую
     * @param id_order
     * @param cb
     */
    @FormUrlEncoded
    @POST("/c_order/cancel")
    void cancelOrder(
            @Field("id_order") long id_order,
            Callback<BaseStruct<Order>> cb
    );


    @FormUrlEncoded
    @POST("/c_order/getOrder")
    void getOrder(
            @Field("id_order") long id_order,
            Callback<BaseStruct<Order>> cb
    );

    @FormUrlEncoded
    @POST("/c_order/getOrdersPosition")
    void getOrdersPosition(
            @Field("empty") String empty,
            Callback<BaseStruct<OrderPosition>> cb
    );

    @FormUrlEncoded
    @POST("/c_order/getOrdersPosition")
    void getOrdersPosition(
            @Field("id_order") long id_order,
            Callback<BaseStruct<OrderPosition>> cb
    );

    /**
     * тестирую
     * @param id_order
     * @param end_time
     * @param message
     * @param can_see
     * @param cb
     */
    @FormUrlEncoded
    @POST("/c_order/updateOrder")
    void updateOrder(
            @Field("id_order") String id_order,
            @Field("end_time") String end_time,
            @Field("message") String message,
            @Field("can_see") String can_see,
            Callback<BaseStruct<Order>> cb
    );

    @FormUrlEncoded
    @POST("/c_order/saveOrderPosition")
    void saveOrderPosition(
            @Field("lat") double lat,
            @Field("lon") double lon,
            Callback<BaseStruct<UserPosition>> cb
    );

    @GET("/c_order/getOrders")
    void getOrders(
            Callback<BaseStruct<Order>> cb
    );

    @FormUrlEncoded
    @POST("/c_order/confirmOrder")
    void confirmOrder(
            @Field("id_order") long id_order,
            Callback<BaseStruct<Order>> cb
    );

    /**
     * Для отладки
     * @param cb
     */
    @GET("/c_order/getOrders")
    void getOrders_str(
            Callback<Response> cb
    );
}
