package com.kolibru.veloportation.adapters;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.databinding.ItemUserBinding;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.web.WebFunctionService;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;


public class UserAdapter extends BaseAdapter<User, UserAdapter.RouteHolder> {

    WebFunctionService webFunctionService;

    public UserAdapter(List<User> source) {
        super(source);
    }

    @Override
    public RouteHolder onCreateViewHolder(View itemView) {
        return new RouteHolder(itemView);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
    }

    @Override
    public void onBindViewHolder(final RouteHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        webFunctionService=WebFunctionService.getInstance(holder.mBinding.card.getContext());
        final User user = getItem(position);
        holder.mBinding.setItem(user);
    }

    static class RouteHolder extends BaseAdapter.ViewHolder {
        ItemUserBinding mBinding;
        View itemView;

        public RouteHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            mBinding = DataBindingUtil.bind(itemView);
        }

    }

}
