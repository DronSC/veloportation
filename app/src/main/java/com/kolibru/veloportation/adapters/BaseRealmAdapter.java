package com.kolibru.veloportation.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;


import com.kolibru.veloportation.R;
import com.kolibru.veloportation.models.Order;

import io.realm.OrderedRealmCollection;
import io.realm.RealmModel;
import io.realm.RealmRecyclerViewAdapter;


public abstract class BaseRealmAdapter<T extends RealmModel, VH extends BaseRealmAdapter.ViewHolder> extends RealmRecyclerViewAdapter<T,VH> implements View.OnClickListener {

    OnItemClickListener mOnItemClickListener;//храним обработчик нажатий

    public BaseRealmAdapter(Context context,OrderedRealmCollection<T> source) {
        super(context,source,true);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.itemView.setTag(R.id.item_tag, position);
    }

    @Override
    public final VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = onCreateView(parent, viewType);
        itemView.setOnClickListener(this);
        return onCreateViewHolder(itemView);
    }

    public abstract View onCreateView(ViewGroup parent, int viewType);

    public abstract VH onCreateViewHolder(View itemView);

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public int getItemIndex(T item){
        return getData().indexOf(item);
    }

    @Override
    public void onClick(View v) {       //когда нажали объект адаптера
        int position = (int) v.getTag(R.id.item_tag);
        if (mOnItemClickListener != null) { //если есть обработчик нажатия - то запускаем его
            mOnItemClickListener.onItemClick(getItem(position), position);
        }
    }

    public interface OnItemClickListener<T> {//интерфейс работы нажатия

        void onItemClick(T item, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }
}
