package com.kolibru.veloportation.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.databinding.ItemOrderBinding;
import com.kolibru.veloportation.models.Order;

import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


public class OrderAdapter extends BaseAdapter<Order, OrderAdapter.RouteHolder> {
    private int state=-999;
    private Realm realm;

    List<Order> all_source;
    public OrderAdapter(List<Order> source, Realm realm) {
        super(source);
        all_source=source;
        this.realm=realm;
    }

    @Override
    public RouteHolder onCreateViewHolder(View itemView) {
        return new RouteHolder(itemView);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
    }


    public void withFilter(int state){
        if(state!=-999) {
            source = realm
                .where(Order.class)
                .equalTo("state", state)
                .findAll();
        }
        else {
            source = realm
                .where(Order.class)
                .findAll();
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final RouteHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Order order = getItem(position);
        holder.mBinding.setItem(order);
        //if(order.getState()!=state && state!=-999){
        //    holder.mBinding.getRoot().setVisibility(View.GONE);
        //}
        //else
        //    holder.mBinding.getRoot().setVisibility(View.VISIBLE);
        switch (order.getState()) {
            case 0:{
                holder.mBinding.state.setText(holder.mBinding.getRoot().getContext().getString(R.string.state_free));
                holder.mBinding.state.setTextColor(ConstClass.getColor(holder.mBinding.getRoot().getContext(),R.color.primary));
                break;
            }
            case 1:{
                holder.mBinding.state.setText(holder.mBinding.getRoot().getContext().getString(R.string.state_start));
                holder.mBinding.state.setTextColor(ConstClass.getColor(holder.mBinding.getRoot().getContext(),R.color.yellow));
                break;
            }
            case 2:{
                holder.mBinding.state.setText(holder.mBinding.getRoot().getContext().getString(R.string.state_finish));
                holder.mBinding.state.setTextColor(ConstClass.getColor(holder.mBinding.getRoot().getContext(),R.color.green));
                break;
            }
            default:{
                holder.mBinding.state.setText(holder.mBinding.getRoot().getContext().getString(R.string.state_canceled));
                holder.mBinding.state.setTextColor(ConstClass.getColor(holder.mBinding.getRoot().getContext(),R.color.accent_gray));
                break;
            }
        }
    }

    class RouteHolder extends BaseAdapter.ViewHolder {
        ItemOrderBinding mBinding;
        View itemView;

        public RouteHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            mBinding = DataBindingUtil.bind(itemView);
        }

    }

}
