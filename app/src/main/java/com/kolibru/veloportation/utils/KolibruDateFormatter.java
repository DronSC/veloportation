package com.kolibru.veloportation.utils;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Weeks;
import org.joda.time.format.DateTimeFormat;

/**
 * Created by Andrej_Maligin on 25.02.2016.
 */
public class KolibruDateFormatter {

    public static String getSimpleTextDateTime(String dateStr){
        DateTime dateTime= DateTime.now();
        try {
            dateTime = DateTime.parse(dateStr, DateTimeFormat.shortDateTime());
        }
        catch (Exception e){
            try {
                dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
            }
            catch (Exception ee){
                try {
                    dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("dd-MM-yyyy HH:mm:ss"));
                }
                catch (Exception eee){
                    try {
                        dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd"));
                    }
                    catch (Exception eeee){
                        return dateStr;
                    }
                }
            }
        }
        return getSimpleTextDateTime(dateTime);
    }

    public static String getSimpleTextDateTime(DateTime dateTime) {
        try {
            return dateTime.toString(DateTimeFormat.forPattern("HH:mm dd MMMM yyyy"));
        }
        catch (Exception e1) {
        }
        return  dateTime.toString(DateTimeFormat.shortDate());
    }

    public static String getSimpleTextDate(String dateStr){
        DateTime dateTime= DateTime.now();
        try {
            dateTime = DateTime.parse(dateStr, DateTimeFormat.shortDateTime());
        }
        catch (Exception e){
            try {
                dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
            }
            catch (Exception ee){
                try {
                    dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss"));
                }
                catch (Exception eee){
                    try {
                        dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd"));
                    }
                    catch (Exception eeee){
                        return dateStr;
                    }
                }
            }
        }
        return getSimpleTextDate(dateTime);
    }

    public static String getSimpleTextDate(DateTime dateTime) {
        try {
            //return dateTime.toString(DateTimeFormat.forPattern("HH:mm dd MMMM yyyy"));
            return dateTime.toString(DateTimeFormat.forPattern("dd MMMM yyyy"));
        }
        catch (Exception e1) {

        }
        return
                dateTime.toString(DateTimeFormat.shortDate());
    }

    public static String getSimpleDateWithMontheText(String dateStr) {
        DateTime dateTime= DateTime.now();

        try {
            dateTime = DateTime.parse(dateStr, DateTimeFormat.shortDateTime());
        }
        catch (Exception e){
            try {
                dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
            }
            catch (Exception ee){
                try {
                    dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss"));
                }
                catch (Exception eee){
                    try {
                        dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd"));
                    }
                    catch (Exception eeee){
                            try {
                                dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSSSS"));
                            }
                            catch (Exception eeeeee){
                                try {
                                    dateTime = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSZ"));
                                }
                                catch (Exception eeeeeee){
                                    return dateStr;
                                }
                            }
                    }
                }
            }
        }
        return getSimpleDateWithMontheText(dateTime);
    }

    public static String getSimpleDateWithMontheText(DateTime dateTime) {
        try {
            return dateTime.toString(DateTimeFormat.forPattern("dd MMMM yyyy"));

        } catch (Exception e1) {
            return dateTime.toString(DateTimeFormat.shortDate());
        }
    }

    public static String getSimpleWeekText(DateTime dateTime) {
        try {
            return dateTime.toString(DateTimeFormat.forPattern("EEEE"));

        } catch (Exception e1) {
            return  dateTime.toString(DateTimeFormat.shortDate());
        }
    }

    public static String getDateAgo(String date) {
        try {
            //Log.e("getDateAgo", published_at.get());
            DateTime last_d = DateTime.now();
            try {
                last_d = DateTime.parse(date, DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss"));
            }
            catch (Exception e){
                try {
                    last_d = DateTime.parse(date, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSSSS"));
                }
                catch (Exception ee){
                    try {
                        last_d = DateTime.parse(date, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSZ"));
                    }
                    catch (Exception eee){
                        last_d = DateTime.parse(date, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
                    }
                }
            }
            DateTime dt_now = DateTime.now();

            int weeks = Math.abs(Weeks.weeksBetween(last_d, dt_now).getWeeks());
            if (weeks == 0) {
                int days = Math.abs(Days.daysBetween(last_d, dt_now).getDays());
                if (days == 0) {
                    int hours = Math.abs(Hours.hoursBetween(last_d, dt_now).getHours());
                    if (hours == 0) {
                        int min = Math.abs(Minutes.minutesBetween(last_d, dt_now).getMinutes());
                        if (min == 0)
                            min = 1;
                        return Integer.toString(min) + " мин. назад";

                    } else
                        return Integer.toString(hours) + " ч. назад";

                } else
                    return Integer.toString(days) + " д. назад";
            } else {
                if (weeks > 2) {
                    return getSimpleTextDateTime(date);
                } else
                    return Integer.toString(weeks) + " нед. назад";
            }
        } catch (Exception e) {
            return getSimpleTextDateTime(date);
        }
    }
}
