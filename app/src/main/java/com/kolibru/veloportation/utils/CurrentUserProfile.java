package com.kolibru.veloportation.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.kolibru.veloportation.activities.Auth;
import com.kolibru.veloportation.activities.MainUser;
import com.kolibru.veloportation.database.OrderModel;
import com.kolibru.veloportation.database.UserModel;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.MediaResource;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.web.WebFunctionService;

import java.util.List;

import io.realm.Realm;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Andrej_Maligin on 25.10.2016.
 */
public class CurrentUserProfile {
    private static CurrentUserProfile ourInstance = new CurrentUserProfile();
    private static Context context;
    private static AppSetting appSetting;
    public static long id=-1;
    public static String surname=null;
    public static String name=null;
    public static String patro=null;
    public static String login=null;
    public static String pass=null;
    public static String mobile=null;
    public static int state=0;
    public static long ava=-1;
    public static int type=-1;

    public static CurrentUserProfile getInstance(Context context) {
        CurrentUserProfile.context=context.getApplicationContext();
        appSetting=AppSetting.getInstance(context);
        return ourInstance;
    }

    private CurrentUserProfile() {
        id=-1;
        surname=null;
        name=null;
        patro=null;
        login=null;
        pass=null;
        mobile=null;
        state=0;
        ava=-1;
        type=-1;
    }

    public String getImageLink() {
        try {
            String link = WebFunctionService.BASE_URL + "/image/" + String.valueOf(ava);
            return link;
        }
        catch (Exception e){
            return WebFunctionService.BASE_URL + "/image/-1";
        }
    }

    public String getFio() {
        StringBuilder stringBuilder=new StringBuilder();
        if(surname!=null) {
            stringBuilder.append(surname);
            stringBuilder.append(" ");
        }
        if(name!=null) {
            stringBuilder.append(name);
            stringBuilder.append(" ");
        }
        if(patro!=null) {
            stringBuilder.append(patro);
        }

        return stringBuilder.toString();
    }

    public void loadUser(long user_id){
        UserModel userModel=new UserModel();
        if(!userModel.getRealm().isClosed()){
            loadUser(userModel.getUser(user_id));
        }
        userModel.close();
    }

    public void loadUser(User user){
        id=user.getId();
        surname=user.getSurname();
        name=user.getName();
        patro=user.getPatro();
        login=user.getLogin();
        if(appSetting.haveKey(AppSetting.SETTING_PASS))
            pass=appSetting.getString(AppSetting.SETTING_PASS);
        mobile=user.getMobile();
        type=user.getType();
        if(type>1)
            state=1;
        else
            state=user.getState();
        if(user.getAva()!=null)
            ava=user.getAva().getId();
    }

    public void closeUser() {
        id=-1;
        surname=null;
        name=null;
        patro=null;
        login=null;
        pass=null;
        mobile=null;
        ava=-1;
        state=0;
    }

    public void removeUser() {
        UserModel userModel=new UserModel();
        if(!userModel.getRealm().isClosed()){
            userModel.removeItem(userModel.getUser(id));
        }
        userModel.close();
        closeUser();
    }

    public boolean logoutUser(){
        appSetting.removeKey(AppSetting.SETTING_SESSION);
        appSetting.removeKey(AppSetting.SETTING_MY_PROFILE_ID);
        appSetting.removeKey(AppSetting.SETTING_LOGIN);
        appSetting.removeKey(AppSetting.SETTING_PASS);
        try {
            OrderModel orderModel=new OrderModel();
            orderModel.getAllItems().deleteAllFromRealm();
            orderModel.close();
        }
        catch (Exception e){
        }
        closeUser();
        return  true;
    }

    public User loginUser(BaseStruct<User> userBaseStruct, Response response){
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equals("Set-Cookie")) {
                String[] cookies = header.getValue().split(";");
                for (String param : cookies) {
                    param = param.trim();
                    String[] key_val = param.split("=");
                    if (key_val[0].equals("veloport")) {
                        AppSetting appSetting=AppSetting.getInstance(context);
                        appSetting.saveSharedPreferences(AppSetting.SETTING_SESSION,key_val[1]);
                        WebFunctionService.setTOKEN(key_val[1]);
                    }
                    break;
                }
            }
        }
        UserModel userModel = new UserModel();
        if (!userModel.getRealm().isClosed()) {
            userModel.saveItems(userBaseStruct.getResult());
            User user = userBaseStruct.getResult().get(0);
            loadUser(user);
            AppSetting appSetting = AppSetting.getInstance(context);
            appSetting.saveSharedPreferences(AppSetting.SETTING_MY_PROFILE_ID, userBaseStruct.getResult().get(0).getId());
            return user;
        } else {
            Toast.makeText(context, userBaseStruct.getMessage(), Toast.LENGTH_LONG).show();
        }
        return null;
    }
}
