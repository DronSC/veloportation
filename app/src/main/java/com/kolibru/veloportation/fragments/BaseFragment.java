package com.kolibru.veloportation.fragments;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.kolibru.veloportation.R;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.web.WebFunctionService;
import com.kolibru.veloportation.web.WebService;


/**
 * Created by artoymtkachenko on 05.07.15.
 */

public class BaseFragment extends Fragment {

    protected WebFunctionService webFunctionService=null;
    protected AppSetting appSetting=null;

    /**
     * Переход на новый фрагмент
     * @param f_new
     */
    public void goToFragment(Fragment f_new) {
        String TAG = f_new.getClass().getSimpleName();

        getActivity().getSupportFragmentManager().popBackStackImmediate();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frag_main, f_new, TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        appSetting=AppSetting.getInstance(getActivity());
        webFunctionService=WebFunctionService.getInstance(getActivity());

    }

    protected  boolean isTurnGPS(){
        try {
            final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                return true;
            }
            return false;
        }
        catch (Exception e){
            return false;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    public boolean haveInternet(){
        return WebFunctionService.isInternetOn(getActivity());
    }

    public StaggeredGridLayoutManager getGridLaytoutManager(int count) {
        return new StaggeredGridLayoutManager(count, StaggeredGridLayoutManager.VERTICAL);
    }


}
