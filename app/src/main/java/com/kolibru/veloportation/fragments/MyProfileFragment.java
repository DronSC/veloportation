package com.kolibru.veloportation.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kolibru.veloportation.App;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.activities.Auth;
import com.kolibru.veloportation.activities.MainUser;
import com.kolibru.veloportation.activities.OneOrderActivity;
import com.kolibru.veloportation.database.UserModel;
import com.kolibru.veloportation.databinding.FragmentMyProfileBinding;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.viewmodel.UserViewModel;
import com.kolibru.veloportation.web.WebFunctionService;

import org.parceler.Parcels;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MyProfileFragment extends BaseFragment {

    private FragmentMyProfileBinding itemBinding;
    private UserModel itemModel;
    private CurrentUserProfile currentUserProfile;
    Handler handler_no_internet = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Snackbar.make(itemBinding.getRoot(), getString(R.string.message_no_internet), Snackbar.LENGTH_LONG).show();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        itemModel=new UserModel();
        currentUserProfile = CurrentUserProfile.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        itemBinding = DataBindingUtil.bind(view);
        User user=itemModel.getUser(currentUserProfile.id);
        itemBinding.setUser(user);
        if(currentUserProfile.type!=-1 && currentUserProfile.type==1)
            itemBinding.type.setText("Курьер");
        else
            itemBinding.type.setText("Заказчик");

        itemBinding.uploadAva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                getActivity().startActivityForResult(pickPhoto, MainUser.CHOOSE_RESULT);
            }
        });

        if(currentUserProfile.ava>0) {
            Glide.with(getActivity())
                    .load(currentUserProfile.getImageLink())
                    .centerCrop()
                    .into(itemBinding.image);
        }
        else {
            Glide.with(getActivity())
                    .load(R.drawable.user)
                    .centerCrop()
                    .into(itemBinding.image);
        }

        itemBinding.changeFio.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ChangeFioDialog.show(getActivity(),currentUserProfile.surname,currentUserProfile.name,currentUserProfile.patro);
                return false;
            }
        });

        itemBinding.changePhone.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ChangePhoneDialog.show(getActivity(),currentUserProfile.mobile);
                return false;
            }
        });

        itemBinding.butChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordDialog.show(getActivity());
            }
        });

        return view;
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {menu.clear();
        inflater.inflate(R.menu.main_empty, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        itemModel.close();
    }
}
