package com.kolibru.veloportation.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.activities.MainUser;
import com.kolibru.veloportation.database.UserModel;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.web.WebFunctionService;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andrej_Maligin on 11.10.2015.
 */
public class ChangePasswordDialog extends SimpleDialogFragment {
    View dialogView = null;
    private UserModel userModel;
    public static String TAG = "ChangePasswordDialog";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userModel=new UserModel();
        setRetainInstance(true);
    }

    public static void show(FragmentActivity activity) {
        Bundle bundle=new Bundle();
        ChangePasswordDialog changeFioDialog=new ChangePasswordDialog();
        changeFioDialog.setArguments(bundle);
        changeFioDialog.show(activity.getSupportFragmentManager(), TAG);
    }

    @Override
    public Builder build(Builder builder) {
        dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.change_password, null);

        final EditText password = (EditText) dialogView.findViewById(R.id.pass);

        final EditText password_repeat = (EditText) dialogView.findViewById(R.id.two_pass);
        builder.setView(dialogView);

        dialogView.findViewById(R.id.but_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String pass=password.getText().toString();
                if (password.getText().length()>5) {
                    if (password.getText().toString().equals(password_repeat.getText().toString())) {
                            final WebFunctionService webFunction = new WebFunctionService();
                            webFunction.updatePass(pass,
                                new Callback<BaseStruct<User>>() {
                                    @Override
                                    public void success(BaseStruct<User> profile_rBaseStruct, Response response) {
                                        try {
                                            if (profile_rBaseStruct.getStatus() > 0) {
                                                userModel.saveItems(profile_rBaseStruct.getResult());
                                                AppSetting appSetting = AppSetting.getInstance(getActivity());
                                                appSetting.saveSharedPreferences(AppSetting.SETTING_PASS, pass);

                                                ChangePasswordDialog.this.dismiss();
                                                ((MainUser) getActivity()).reload();
                                            } else {
                                                Toast.makeText(getActivity(), profile_rBaseStruct.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(getActivity(), profile_rBaseStruct.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("updatePass", "ERROR:" + ConstClass.responseToString(error.getResponse()));
                                    }
                            });
                    } else {
                        Toast.makeText(getActivity(), "Пароль и его повторение - не совпадают", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getActivity(), "Пароль должен быть длиннее 6 символов!", Toast.LENGTH_LONG).show();
                }
            }
        });

        dialogView.findViewById(R.id.but_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordDialog.this.dismiss();
            }
        });
        return builder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        userModel.close();
    }
}