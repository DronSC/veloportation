package com.kolibru.veloportation.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.activities.CreateOrder;
import com.kolibru.veloportation.activities.FreeMap;
import com.kolibru.veloportation.activities.OneOrderActivity;
import com.kolibru.veloportation.activities.Where52Activity;
import com.kolibru.veloportation.adapters.OrderAdapter;
import com.kolibru.veloportation.database.OrderModel;
import com.kolibru.veloportation.databinding.FragmentOrdersBinding;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.viewmodel.OrderViewModel;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.sevenheaven.segmentcontrol.SegmentControl;

import org.parceler.Parcels;

import java.util.ArrayList;

import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class OrdersFragment extends BaseFragment implements OnRefreshListener, OnMoreListener {

    private FragmentOrdersBinding itemBinding;
    private OrderViewModel itemViewModel;
    private OrderModel orderModel;
    CurrentUserProfile currentUserProfile;
    private int orderState=-999;

    Handler handler_no_internet = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                Snackbar.make(itemBinding.getRoot(), getString(R.string.message_no_internet), Snackbar.LENGTH_LONG).show();
            }catch (Exception e){

            }
        }
    };


    Handler handler_snackbar = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                Snackbar.make(itemBinding.getRoot(), msg.getData().getString("message"), Snackbar.LENGTH_LONG).show();
            }catch (Exception e){

            }
        }
    };

    Handler handler_start_loader = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        try {
            itemBinding.routesList.getSwipeToRefresh().post(new Runnable() {
                @Override
                public void run() {
                    itemBinding.routesList.getSwipeToRefresh().setRefreshing(true);
                }
            });
        }catch (Exception e){

        }
        }
    };

    Handler handler_stop_loader = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        try {
            itemBinding.routesList.getSwipeToRefresh().setRefreshing(false);
            itemBinding.routesList.getMoreProgressView().setVisibility(View.GONE);
        }catch (Exception e){

        }
        }
    };


    private Callback<BaseStruct<Order>> loadItems=new Callback<BaseStruct<Order>>() {
        @Override
        public void success(BaseStruct<Order> structRoutes, Response response) {
            try{
                if(structRoutes.getStatus()>=0) {
                    orderModel.saveItems(structRoutes.getResult());
                    itemViewModel.reloadItems(orderModel.getAllItems());
                }
                else {
                    Message msg = handler_snackbar.obtainMessage();
                    Bundle bundle=new Bundle();
                    bundle.putString("message",structRoutes.getMessage());
                    msg.setData(bundle);
                    handler_snackbar.sendMessage(msg);
                }
            }
            catch (Exception e){
                Log.e("getRoutes Exception","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
            }
            handler_stop_loader.sendEmptyMessage(0);
        }

        @Override
        public void failure(RetrofitError error) {
            Log.e("ERROR",ConstClass.responseToString(error.getResponse()));
            handler_stop_loader.sendEmptyMessage(0);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        orderModel=new OrderModel();
        currentUserProfile=CurrentUserProfile.getInstance(getActivity());
        itemViewModel = new OrderViewModel(getActivity(), orderModel.getAllItems());
        itemViewModel.setOnItemClickListener(new OrderViewModel.OnItemClickListener<Order>() {
            @Override
            public void onItemClick(Order item, View view) {
                Intent intent = new Intent(getActivity(), OneOrderActivity.class);
                Parcelable wrap = Parcels.wrap(Order.class, item);
                intent.putExtra(Order.class.getSimpleName(), wrap);
                startActivity(intent);
            }

        });
    }

    public OrderAdapter getAdapter(int count){
        itemBinding.routesList.setLayoutManager(new StaggeredGridLayoutManager(count, StaggeredGridLayoutManager.VERTICAL));
        if(haveInternet()){
            loadFromInet(0);
        }
        else{
            handler_no_internet.sendEmptyMessage(0);
        }

        return itemViewModel.getAdapter();
    }

    public void setAdapter(){
        itemBinding.routesList.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        if(haveInternet()){
            loadFromInet(0);
        }
        else{
            handler_no_internet.sendEmptyMessage(0);
        }
        itemBinding.routesList.setAdapter(itemViewModel.getAdapter());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        itemBinding = DataBindingUtil.bind(view);
        itemBinding.setBaseFragment(this);
        //itemBinding.routesList.setupMoreListener(this, 10);
        itemBinding.butSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Where52Activity.class));
            }
        });
        //itemBinding.routesList.setDrawingCacheEnabled(true);
        //itemBinding.routesList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        itemBinding.routesList.setRefreshListener(this);

        //itemBinding.bottomSheet.setFab(itemBinding.fab);
        //itemBinding.fab.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        itemBinding.bottomSheet.expandFab();
        //    }
       // });

        switch (currentUserProfile.type){
            case 2:{
                itemBinding.fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (appSetting.haveKey(AppSetting.SETTING_MY_PROFILE_ID)) {
                         //   AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                          //  builder.setTitle("Вы желаете создать новый заказ?")
                          //          .setCancelable(false)
                         //           .setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                        //                public void onClick(DialogInterface dialog, int id) {
                                            Intent intent = new Intent(getActivity(), CreateOrder.class);
                                            startActivity(intent);
                         //                   dialog.cancel();
                         //               }
                        //            })
                         //           .setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                         //               public void onClick(DialogInterface dialog, int id) {
                         //                   dialog.cancel();
                        //                }
                         //           });
                        //    AlertDialog alert = builder.create();
                        //    alert.show();
                        }
                        else{
                            Toast.makeText(getActivity(),"Вы не авторизованы!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                /*itemBinding.map.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            startActivity(new Intent(getActivity(),FreeMap.class));
                    }
                });
                itemBinding.map.setVisibility(View.VISIBLE);
                itemBinding.add.setVisibility(View.VISIBLE);
                itemBinding.bottomSheet.setVisibility(View.VISIBLE);*/
                itemBinding.fab.setVisibility(View.VISIBLE);
                break;
            }
            default:{
                //itemBinding.map.setVisibility(View.GONE);
                //itemBinding.add.setVisibility(View.GONE);
                //itemBinding.bottomSheet.setVisibility(View.GONE);
                itemBinding.fab.setVisibility(View.GONE);
            }
        }

        itemBinding.segmentControl.setOnSegmentControlClickListener(new SegmentControl.OnSegmentControlClickListener() {
            @Override
            public void onSegmentControlClick(int index) {
                switch (index){
                    case 0:
                        orderState=-999;
                        break;
                    case 1:
                        orderState=0;
                        break;
                    case 2:
                        orderState=1;
                        break;
                    case 3:
                        orderState=2;
                        break;
                }
                itemViewModel.changeState(orderState);
            }
        });
        if(currentUserProfile.state<1){
            itemBinding.blockConfirm.setVisibility(View.VISIBLE);
            itemBinding.fab.setVisibility(View.GONE);
        }
        else {
            itemBinding.blockConfirm.setVisibility(View.GONE);
        }
        return view;
    }


    private void loadFromInet(int position){
        //handler_start_loader.sendEmptyMessage(0);
        webFunctionService.getOrders(loadItems);
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        itemBinding.routesList.getSwipeToRefresh().setRefreshing(false);
        itemBinding.routesList.getMoreProgressView().setVisibility(View.GONE);
        if((overallItemsCount % 5) == 0 && overallItemsCount % itemsBeforeMore ==0)
            loadFromInet(overallItemsCount-1);
    }

    @Override
    public void onRefresh() {
        if (haveInternet()){
            orderModel.clearItems();
            loadFromInet(0);
        } else {
            //itemViewModel.reloadItems(orderModel.getAllItems());
            Snackbar.make(itemBinding.routesList, getString(R.string.message_no_internet), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            handler_stop_loader.sendEmptyMessage(0);
        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {menu.clear();
        inflater.inflate(R.menu.main_empty, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        orderModel.close();
    }
}
