package com.kolibru.veloportation.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.activities.Auth;
import com.kolibru.veloportation.database.OrderModel;
import com.kolibru.veloportation.database.UserModel;
import com.kolibru.veloportation.databinding.FragmentAboutBinding;
import com.kolibru.veloportation.databinding.FragmentSettingBinding;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.web.WebFunctionService;

import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SettingFragment extends BaseFragment {

    FragmentSettingBinding itemBinding;
    private CurrentUserProfile currentUserProfile;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        currentUserProfile = CurrentUserProfile.getInstance(getActivity());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        itemBinding = DataBindingUtil.bind(view);

        itemBinding.butChangeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyProfileFragment f_new = new MyProfileFragment();
                goToFragment(f_new);
            }
        });
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = "Версия " + pInfo.versionName;
            itemBinding.version.setText(version);
        } catch (Exception e) {
        }
        switch(currentUserProfile.state){
            case -1:
                itemBinding.state.setText("Забанен");
                break;
            case 1:
                itemBinding.state.setText("Подтвержден");
                break;
            case 0:
                itemBinding.state.setText("Не подтвержден");
                break;
        }
        itemBinding.butExitProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getString(R.string.question_need_login))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if(WebFunctionService.isInternetOn(getActivity())) {
                                    webFunctionService.logout(new Callback<BaseStruct<User>>() {
                                        @Override
                                        public void success(BaseStruct<User> userBaseStruct, Response response) {
                                            currentUserProfile.logoutUser();
                                            startActivity(new Intent(getActivity(), Auth.class));
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            Snackbar
                                                    .make(itemBinding.getRoot(),getString(R.string.error_with_connect),Snackbar.LENGTH_LONG)
                                                    .setActionTextColor(ConstClass.getColor(getActivity(),R.color.white))
                                                    .show();
                                            currentUserProfile.logoutUser();
                                            startActivity(new Intent(getActivity(), Auth.class));
                                            getActivity().finish();
                                        }
                                    });
                                }
                                else {
                                    Snackbar
                                            .make(itemBinding.getRoot(),getString(R.string.error_no_internet),Snackbar.LENGTH_LONG)
                                            .setActionTextColor(ConstClass.getColor(getActivity(),R.color.white))
                                            .show();
                                }
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.main_empty, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
