package com.kolibru.veloportation.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.activities.MainUser;
import com.kolibru.veloportation.database.UserModel;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.web.WebFunctionService;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andrej_Maligin on 11.10.2015.
 */
public class ChangeFioDialog extends SimpleDialogFragment {
    View dialogView = null;
    private UserModel userModel;
    public static String TAG = "ChangeFioDialog";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userModel=new UserModel();
        setRetainInstance(true);
    }

    public static void show(FragmentActivity activity, String last_surname, String last_name, String last_patro) {
        Bundle bundle=new Bundle();
        bundle.putString("last_surname",last_surname);
        bundle.putString("last_name",last_name);
        bundle.putString("last_patro",last_patro);
        ChangeFioDialog changeFioDialog=new ChangeFioDialog();
        changeFioDialog.setArguments(bundle);
        changeFioDialog.show(activity.getSupportFragmentManager(), TAG);
    }

    @Override
    public Builder build(Builder builder) {
        dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.change_fio, null);
        String last_surname=getArguments().getString("last_surname");
        String last_name=getArguments().getString("last_name");
        String last_patro=getArguments().getString("last_patro");

        final EditText surname = (EditText) dialogView.findViewById(R.id.surname);
            surname.setText(last_surname);
        final EditText name = (EditText) dialogView.findViewById(R.id.name);
            name.setText(last_name);
        final EditText patro = (EditText) dialogView.findViewById(R.id.patro);
            patro.setText(last_patro);
        builder.setView(dialogView);

        dialogView.findViewById(R.id.but_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (surname.getText().length()>0 && name.getText().length()>0) {
                    final WebFunctionService webFunction = new WebFunctionService();
                    webFunction.updateFio(surname.getText().toString(),name.getText().toString(),patro.getText().toString(),
                            new Callback<BaseStruct<User>>() {
                                @Override
                                public void success(BaseStruct<User> profile_rBaseStruct, Response response) {
                                    try{
                                        if (profile_rBaseStruct.getStatus() > 0) {
                                            userModel.saveItems(profile_rBaseStruct.getResult());
                                            ChangeFioDialog.this.dismiss();
                                            ((MainUser)getActivity()).reload();
                                        }
                                        else{
                                            Toast.makeText(getActivity(), profile_rBaseStruct.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    catch (Exception e){
                                        Toast.makeText(getActivity(), profile_rBaseStruct.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Log.e("updateFio", "ERROR:" + ConstClass.responseToString(error.getResponse()));
                                }
                            });
                }
                else
                    Toast.makeText(getActivity(), "Пустые поля!", Toast.LENGTH_LONG).show();
            }
        });

        dialogView.findViewById(R.id.but_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeFioDialog.this.dismiss();
            }
        });
        return builder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        userModel.close();
    }
}