package com.kolibru.veloportation.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.activities.MainUser;
import com.kolibru.veloportation.database.UserModel;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.web.WebFunctionService;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andrej_Maligin on 11.10.2015.
 */
public class ChangePhoneDialog extends SimpleDialogFragment {
    View dialogView = null;
    private UserModel userModel;
    public static String TAG = "ChangePhoneDialog";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userModel=new UserModel();
        setRetainInstance(true);
    }

    public static void show(FragmentActivity activity, String last_facebook) {
        Bundle bundle=new Bundle();
        bundle.putString("last_phone",last_facebook);
        ChangePhoneDialog changeFioDialog=new ChangePhoneDialog();
        changeFioDialog.setArguments(bundle);
        changeFioDialog.show(activity.getSupportFragmentManager(), TAG);
    }

    @Override
    public Builder build(Builder builder) {
        //builder.setTitle("Изменение пароля");
        dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.change_line, null);
        String last_facebook=getArguments().getString("last_phone");

        final EditText value = (EditText) dialogView.findViewById(R.id.value);
        TextView title=(TextView)dialogView.findViewById(R.id.type_line);
        title.setText(getString(R.string.text_mobile));
        value.setText(last_facebook);
        builder.setView(dialogView);

        dialogView.findViewById(R.id.but_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConstClass.isValidPhone(value.getText().toString())) {
                    final WebFunctionService webFunction = new WebFunctionService();
                    webFunction.updateContact(value.getText().toString(),
                            new Callback<BaseStruct<User>>() {
                                @Override
                                public void success(BaseStruct<User> profile_rBaseStruct, Response response) {
                                    try{
                                        if (profile_rBaseStruct.getStatus() > 0) {
                                            userModel.saveItems(profile_rBaseStruct.getResult());
                                            ChangePhoneDialog.this.dismiss();
                                            ((MainUser)getActivity()).reload();
                                        }
                                        else{
                                            Toast.makeText(getActivity(), profile_rBaseStruct.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    catch (Exception e){
                                        Toast.makeText(getActivity(), profile_rBaseStruct.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Log.e("updateContact", "ERROR:" + ConstClass.responseToString(error.getResponse()));
                                }
                            });
                }
                else
                    Toast.makeText(getActivity(), "Некорректная запись мобильного телефона!", Toast.LENGTH_LONG).show();
            }
        });

        dialogView.findViewById(R.id.but_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePhoneDialog.this.dismiss();
            }
        });
        return builder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        userModel.close();
    }
}