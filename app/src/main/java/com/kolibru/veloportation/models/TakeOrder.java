package com.kolibru.veloportation.models;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.TakeOrderRealmProxy;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
@RealmClass      // required if using JDK 1.6 (unrelated to Parceler issue)
@Parcel(implementations = {TakeOrderRealmProxy.class},value = Parcel.Serialization.BEAN, analyze = {TakeOrder.class})
public class TakeOrder extends RealmObject  {

    @PrimaryKey
    private long id;
    private String start_time;
    private String end_time;
    private String message;
    private int can_see;
    private String create_date;
    private long create_user;
    private String take_fio;
    private String take_rating;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public long getCreate_user() {
        return create_user;
    }

    public void setCreate_user(long create_user) {
        this.create_user = create_user;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public int getCan_see() {
        return can_see;
    }

    public void setCan_see(int can_see) {
        this.can_see = can_see;
    }

    public String getTake_fio() {
        return take_fio;
    }

    public void setTake_fio(String take_fio) {
        this.take_fio = take_fio;
    }

    public String getTake_rating() {
        return take_rating;
    }

    public void setTake_rating(String take_rating) {
        this.take_rating = take_rating;
    }
}



