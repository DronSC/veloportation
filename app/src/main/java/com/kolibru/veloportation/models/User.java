package com.kolibru.veloportation.models;

import org.parceler.Parcel;
import org.parceler.ParcelProperty;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.UserRealmProxy;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
@RealmClass      // required if using JDK 1.6 (unrelated to Parceler issue)
@Parcel(implementations = {UserRealmProxy.class},value = Parcel.Serialization.BEAN, analyze = {User.class})
public class User extends RealmObject  {

    @PrimaryKey
    private long id;
    private String surname;
    private String name;
    private String patro;
    private String login;
    private String pass;
    private String mobile;
    private String rating;
    private int state;
    private MediaResource ava;
    private int type;
    @Ignore
    private String fio;

    private String id_user_notify;


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatro() {
        return patro;
    }

    public void setPatro(String patro) {
        this.patro = patro;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getFio() {
        return getSurname()+(getName()!=null?" "+getName():"")+(getPatro()!=null?" "+getPatro():"");
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getId_user_notify() {
        return id_user_notify;
    }

    public void setId_user_notify(String id_user_notify) {
        this.id_user_notify = id_user_notify;
    }

    public MediaResource getAva() {
        return ava;
    }

    @ParcelProperty("ava")
    public void setAva(MediaResource ava) {
        this.ava = ava;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}



