package com.kolibru.veloportation.models;

import org.parceler.Parcel;
import org.parceler.ParcelProperty;

import io.realm.OrderRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
@RealmClass
@Parcel(implementations = {OrderRealmProxy.class},value = Parcel.Serialization.BEAN, analyze = {Order.class})
public class Order extends RealmObject  {

    @PrimaryKey
    private long id;
    private String phone;
    private PlacePosition address_to;
    private PlacePosition address_from;
    private TakeOrder take_order;
    private String message;
    private int state;
    private String create_date;
    private String wait_time;
    private long create_user;
    private double cost;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public long getCreate_user() {
        return create_user;
    }

    public void setCreate_user(long create_user) {
        this.create_user = create_user;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public PlacePosition getAddress_to() {
        return address_to;
    }

    @ParcelProperty("address_to")
    public void setAddress_to(PlacePosition address_to) {
        this.address_to = address_to;
    }

    public PlacePosition getAddress_from() {
        return address_from;
    }

    @ParcelProperty("address_from")
    public void setAddress_from(PlacePosition address_from) {
        this.address_from = address_from;
    }

    public TakeOrder getTake_order() {
        return take_order;
    }

    @ParcelProperty("take_order")
    public void setTake_order(TakeOrder take_order) {
        this.take_order = take_order;
    }

    public String getWait_time() {
        return wait_time;
    }

    public void setWait_time(String wait_time) {
        this.wait_time = wait_time;
    }
}



