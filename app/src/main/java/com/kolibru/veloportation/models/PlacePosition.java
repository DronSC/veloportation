package com.kolibru.veloportation.models;

import org.parceler.Parcel;

import io.realm.OrderRealmProxy;
import io.realm.PlacePositionRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
@RealmClass
@Parcel(implementations = {PlacePositionRealmProxy.class},value = Parcel.Serialization.BEAN, analyze = {PlacePosition.class})
public class PlacePosition extends RealmObject  {

    @PrimaryKey
    private String id;
    private String name;
    private double latitude;
    private double longitude;
    private int type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}



