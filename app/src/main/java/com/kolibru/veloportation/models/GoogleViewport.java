package com.kolibru.veloportation.models;


/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
public class GoogleViewport  {

    private GoogleLocation northeast;
    private GoogleLocation southwest;

    public GoogleLocation getNortheast() {
        return northeast;
    }

    public void setNortheast(GoogleLocation northeast) {
        this.northeast = northeast;
    }

    public GoogleLocation getSouthwest() {
        return southwest;
    }

    public void setSouthwest(GoogleLocation southwest) {
        this.southwest = southwest;
    }
}
