package com.kolibru.veloportation.models;

import java.util.List;


/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
public class GooglePositionInfo{

    private String place_id;
    private String formatted_address;
    private Geometry geometry;
    private List<AdressComponents> address_components;


    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public List<AdressComponents> getAddress_components() {
        return address_components;
    }

    public void setAddress_components(List<AdressComponents> address_components) {
        this.address_components = address_components;
    }
}
