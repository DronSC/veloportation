package com.kolibru.veloportation.models;

import java.util.List;

/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
public class StructGooglePosition {

    private String status;
    private List<GooglePositionInfo> results;


    public List<GooglePositionInfo> getResult() {
        return results;
    }

    public void setResult(List<GooglePositionInfo> result) {
        this.results = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
