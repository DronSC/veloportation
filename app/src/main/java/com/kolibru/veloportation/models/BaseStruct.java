package com.kolibru.veloportation.models;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 *
 */
public class BaseStruct<T extends RealmObject>  {

    private int status;
    private String message;
    private RealmList<T> array;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public RealmList<T> getResult() {
        return array;
    }

    public void setResult(RealmList<T> array) {
        this.array = array;
    }
}



