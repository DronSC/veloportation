package com.kolibru.veloportation.models;

import org.parceler.Parcel;
import org.parceler.ParcelProperty;

import io.realm.OrderPositionRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 *
 */
@RealmClass      // required if using JDK 1.6 (unrelated to Parceler issue)
@Parcel(implementations = {OrderPositionRealmProxy.class},value = Parcel.Serialization.BEAN, analyze = {OrderPosition.class})
public class OrderPosition extends  RealmObject  {

    private Order order;
    private UserPosition position;
    private User courier;


    public Order getOrder() {
        return order;
    }

    @ParcelProperty("order")
    public void setOrder(Order order) {
        this.order = order;
    }

    public UserPosition getPosition() {
        return position;
    }

    @ParcelProperty("position")
    public void setPosition(UserPosition position) {
        this.position = position;
    }

    public User getCourier() {
        return courier;
    }

    @ParcelProperty("courier")
    public void setCourier(User courier) {
        this.courier = courier;
    }
}



