package com.kolibru.veloportation.models;

import org.parceler.Parcel;
import org.parceler.ParcelProperty;

import io.realm.RealmObject;
import io.realm.UserPositionRealmProxy;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Andrej_Maligin on 04.12.15.
 */
@RealmClass
@Parcel(implementations = {UserPositionRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {UserPosition.class})
public class UserPosition extends RealmObject {

    @PrimaryKey
    private long id_user;
    private double lat;
    private double lon;
    private String create_date;
    private String create_user;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public long getId_user() {
        return id_user;
    }

    public void setId_user(long id_user) {
        this.id_user = id_user;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }
}
