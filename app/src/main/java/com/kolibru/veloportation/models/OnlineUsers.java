package com.kolibru.veloportation.models;

import org.parceler.Parcel;

import io.realm.OnlineUsersRealmProxy;
import io.realm.PlacePositionRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
@RealmClass
@Parcel(implementations = {OnlineUsersRealmProxy.class},value = Parcel.Serialization.BEAN, analyze = {OnlineUsers.class})
public class OnlineUsers extends RealmObject  {

    private String count_online;

    public String getCount_online() {
        return count_online;
    }

    public void setCount_online(String count_online) {
        this.count_online = count_online;
    }
}



