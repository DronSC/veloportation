package com.kolibru.veloportation.models;

import org.parceler.Parcel;

import io.realm.MediaResourceRealmProxy;
import io.realm.RealmObject;
import io.realm.TakeOrderRealmProxy;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
@RealmClass      // required if using JDK 1.6 (unrelated to Parceler issue)
@Parcel(implementations = {MediaResourceRealmProxy.class},value = Parcel.Serialization.BEAN, analyze = {MediaResource.class})
public class MediaResource extends RealmObject  {

    @PrimaryKey
    private long id;
    private String name;
    private String type;
    private String  create_date;
    private String  create_user;
    private String url;
    private int visibility;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }
}



