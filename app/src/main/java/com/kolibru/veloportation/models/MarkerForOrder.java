package com.kolibru.veloportation.models;

import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Andrej_Maligin on 27.11.2016.
 */

public class MarkerForOrder {

    private MarkerOptions marker_from;
    private MarkerOptions marker_to;
    private MarkerOptions courier;

    public MarkerForOrder( ){
        this.marker_from=null;
        this.marker_to=null;
        this.courier=null;
    }

    public MarkerForOrder( MarkerOptions marker_from,MarkerOptions marker_to,MarkerOptions courier){
        this.marker_from=marker_from;
        this.marker_to=marker_to;
        this.courier=courier;
    }

    public MarkerOptions getMarker_from() {
        return marker_from;
    }

    public void setMarker_from(MarkerOptions marker_from) {
        this.marker_from = marker_from;
    }

    public MarkerOptions getMarker_to() {
        return marker_to;
    }

    public void setMarker_to(MarkerOptions marker_to) {
        this.marker_to = marker_to;
    }

    public MarkerOptions getCourier() {
        return courier;
    }

    public void setCourier(MarkerOptions courier) {
        this.courier = courier;
    }

}
