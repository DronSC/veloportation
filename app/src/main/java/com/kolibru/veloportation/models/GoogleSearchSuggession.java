package com.kolibru.veloportation.models;

import android.location.Location;
import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Andrej_Maligin on 29.10.2016.
 */

public class GoogleSearchSuggession implements SearchSuggestion {

    private String name;
    private double latitude;
    private double longitude;
    private boolean mIsHistory = false;

    public GoogleSearchSuggession(String suggestion) {
        this.name = suggestion;//.toLowerCase();
    }

    public GoogleSearchSuggession(String suggestion,LatLng location) {
        this.name = suggestion;//.toLowerCase();
        this.latitude = location.latitude;
        this.longitude = location.longitude;
    }

    public GoogleSearchSuggession(Parcel source) {
        this.name = source.readString();
        this.latitude = source.readDouble();
        this.longitude = source.readDouble();
        this.mIsHistory = source.readInt() != 0;
    }

    public void setIsHistory(boolean isHistory) {
        this.mIsHistory = isHistory;
    }

    public boolean getIsHistory() {
        return this.mIsHistory;
    }

    @Override
    public String getBody() {
        return name;
    }

    public static final Creator<GoogleSearchSuggession> CREATOR = new Creator<GoogleSearchSuggession>() {
        @Override
        public GoogleSearchSuggession createFromParcel(Parcel in) {
            return new GoogleSearchSuggession(in);
        }

        @Override
        public GoogleSearchSuggession[] newArray(int size) {
            return new GoogleSearchSuggession[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeInt(mIsHistory ? 1 : 0);
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}