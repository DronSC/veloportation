package com.kolibru.veloportation.models;

/**
 * Created by Andrej_Maligin on 28.11.2015.
 */
public class Geometry  {

    private GoogleLocation location;
    private GoogleViewport viewport;
    private String location_type;


    public GoogleLocation getLocation() {
        return location;
    }

    public void setLocation(GoogleLocation location) {
        this.location = location;
    }

    public GoogleViewport getViewport() {
        return viewport;
    }

    public void setViewport(GoogleViewport viewport) {
        this.viewport = viewport;
    }

    public String getLocation_type() {
        return location_type;
    }

    public void setLocation_type(String location_type) {
        this.location_type = location_type;
    }
}
