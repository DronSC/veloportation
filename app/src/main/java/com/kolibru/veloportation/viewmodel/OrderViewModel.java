package com.kolibru.veloportation.viewmodel;

import android.content.Context;

import com.kolibru.veloportation.adapters.OrderAdapter;
import com.kolibru.veloportation.models.Order;

import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;


public class OrderViewModel extends BaseViewModel<Order,OrderAdapter> {

    protected List<Order> all_items = new ArrayList<>();

    public OrderViewModel(Context context, List<Order> source) {
        setmAdapter(new OrderAdapter(source, Realm.getDefaultInstance()));
    }

    public void reloadItems(List<Order> list,int state) {
        items.clear();
        items.addAll(list);
        getAdapter().withFilter(state);
    }

    public void changeState(int state) {
        getAdapter().withFilter(state);
    }
}
