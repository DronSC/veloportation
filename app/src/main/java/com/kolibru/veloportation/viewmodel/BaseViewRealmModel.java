package com.kolibru.veloportation.viewmodel;

import android.databinding.BaseObservable;


import com.kolibru.veloportation.adapters.BaseRealmAdapter;

import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.RealmList;
import io.realm.RealmModel;

/**
 * Created by Малыгин Андрей on 11.01.2016.
 */
public abstract class BaseViewRealmModel<ITEM extends RealmModel,ADAPTER> extends BaseObservable {

    protected OrderedRealmCollection<ITEM> items = new RealmList<>();
    protected BaseRealmAdapter mAdapter=null;
    protected OnItemClickListener onItemClickListener=null;

    public void setmAdapter(BaseRealmAdapter baseAdapter){
        this.mAdapter = baseAdapter;
        mAdapter.setOnItemClickListener(new BaseRealmAdapter.OnItemClickListener<ITEM>() {

            @Override
            public void onItemClick(ITEM item, int position) {
                if(onItemClickListener!=null)
                    onItemClickListener.onItemClick(item);
            }
        });
        mAdapter.notifyDataSetChanged();
    }

    public interface OnItemClickListener<T> {
        void onItemClick(T item);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public ADAPTER getAdapter(){
        return (ADAPTER)mAdapter;
    }

    public void reloadItems(List<ITEM> list) {
        items.clear();
        items.addAll(list);
        mAdapter.notifyDataSetChanged();
    }


    public void addItems(OrderedRealmCollection<ITEM> list) {
        items.clear();
        items.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    public void addItem(ITEM list) {
        items.add(list);
        mAdapter.notifyDataSetChanged();
    }

    public void clearItems() {
        items.clear();
        mAdapter.notifyDataSetChanged();
    }


    public OrderedRealmCollection<ITEM> getItems() {
        return items;
    }

    public int getItemCount() {
        return items.size();
    }
}
