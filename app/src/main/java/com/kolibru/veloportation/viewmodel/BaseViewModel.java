package com.kolibru.veloportation.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;
import android.view.View;


import com.kolibru.veloportation.adapters.BaseAdapter;

import java.util.List;

/**
 * Created by Малыгин Андрей on 11.01.2016.
 */
public abstract class BaseViewModel<ITEM,ADAPTER> extends BaseObservable {

    @Bindable
    protected ObservableArrayList<ITEM> items = new ObservableArrayList<>();
    protected BaseAdapter mAdapter=null;
    protected OnItemClickListener onItemClickListener=null;

    public void setmAdapter(BaseAdapter baseAdapter){
        this.mAdapter = baseAdapter;
        mAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<ITEM>() {
            @Override
            public void onItemClick(ITEM item, View view) {
                if(onItemClickListener!=null)
                    onItemClickListener.onItemClick(item,view);
            }
        });
        mAdapter.notifyDataSetChanged();
    }



    public interface OnItemClickListener<T> {
        void onItemClick(T item, View view);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public ADAPTER getAdapter(){
        return (ADAPTER)mAdapter;
    }

    public void reloadItems(List<ITEM> list) {
        items.clear();
        items.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    public void clearItems() {
        items.clear();
        mAdapter.notifyDataSetChanged();
    }


    public int getItemCount() {
        return items.size();
    }
}
