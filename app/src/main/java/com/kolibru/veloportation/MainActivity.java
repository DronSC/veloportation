package com.kolibru.veloportation;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.kolibru.veloportation.activities.Auth;
import com.kolibru.veloportation.activities.BaseActivity;
import com.kolibru.veloportation.activities.MainUser;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.web.WebFunctionService;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends BaseActivity {

    Handler handler_go_auth = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            startActivity(new Intent(MainActivity.this, Auth.class));
            MainActivity.this.finish();
        }
    };


    Handler handler_go_profile = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            webFunctionService.auth(appSetting.getString(AppSetting.SETTING_LOGIN), appSetting.getString(AppSetting.SETTING_PASS), new retrofit.Callback<BaseStruct<User>>() {
                @Override
                public void success(BaseStruct<User> userBaseStruct, Response response) {
                    CurrentUserProfile currentUserProfile = CurrentUserProfile.getInstance(getActivity());
                    if(userBaseStruct.getStatus()==1) {
                        if (currentUserProfile.loginUser(userBaseStruct, response)!=null) {
                            currentUserProfile.loadUser(userBaseStruct.getResult().get(0));
                            appSetting.saveSharedPreferences(AppSetting.SETTING_MY_PROFILE_ID,userBaseStruct.getResult().get(0).getId());
                            startActivity(new Intent(MainActivity.this, MainUser.class));
                            MainActivity.this.finish();
                        }
                        else{
                            Toast.makeText(getApplicationContext(),userBaseStruct.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        try {
                            currentUserProfile.closeUser();
                            appSetting.removeKey(AppSetting.SETTING_SESSION);
                            appSetting.removeKey(AppSetting.SETTING_MY_PROFILE_ID);
                            appSetting.removeKey(AppSetting.SETTING_LOGIN);
                            appSetting.removeKey(AppSetting.SETTING_PASS);
                        }
                        catch (Exception e){

                        }

                        Toast.makeText(getApplicationContext(),userBaseStruct.getMessage(),Toast.LENGTH_LONG).show();
                        startActivity(new Intent(MainActivity.this, Auth.class));
                        MainActivity.this.finish();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("ERROR",ConstClass.responseToString(error.getResponse()));
                    startActivity(new Intent(MainActivity.this, MainUser.class));
                    MainActivity.this.finish();
                }
            });

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = "Версия " + pInfo.versionName;
            ((TextView)findViewById(R.id.version)).setText(version);
        } catch (Exception e) {
        }

        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    if (appSetting.haveKey(AppSetting.SETTING_SESSION)
                            && appSetting.haveKey(AppSetting.SETTING_MY_PROFILE_ID)
                            && appSetting.haveKey(AppSetting.SETTING_LOGIN)
                            && appSetting.haveKey(AppSetting.SETTING_PASS)) {
                        WebFunctionService.setTOKEN(appSetting.getString(AppSetting.SETTING_SESSION));
                        CurrentUserProfile currentUserProfile=CurrentUserProfile.getInstance(getActivity());
                        currentUserProfile.loadUser(appSetting.getLong(AppSetting.SETTING_MY_PROFILE_ID));
                        handler_go_profile.sendEmptyMessage(0);
                        return;
                    }
                } catch (Exception e) {
                    Log.e(getLogName("error session"), "error");
                }
                handler_go_auth.sendEmptyMessage(0);
            }
        };
        Thread mythread = new Thread(runnable);
        mythread.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
