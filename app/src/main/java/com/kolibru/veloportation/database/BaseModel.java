package com.kolibru.veloportation.database;

import android.util.Log;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Малыгин Андрей on 11.01.2016.
 */
public abstract class BaseModel<T extends RealmObject> implements InterfaceModel<T> {

    Realm realm;
    public Realm getRealm(){
        return realm;
    }

    public BaseModel() {
        realm= Realm.getDefaultInstance();
    }

    public void open() {
        realm= Realm.getDefaultInstance();
    }

    public void close() {
        realm.close();
    }

    public boolean saveItems(T item) {
        realm.beginTransaction();
        try {
            realm.copyToRealmOrUpdate(item);
            realm.commitTransaction();
            return true;
        }
        catch (Exception e){
            Log.e("saveItem","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
        realm.commitTransaction();
        return false;
    }

    public boolean saveItems(final List<T> item) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(item);
            }
        });
        return true;
    }

    public boolean saveItems(final RealmList<T> item) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(item);
            }

        });
        return true;
    }



    public boolean saveItemsAscync(final RealmList<T> item) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(item);
            }
        });
        return true;
    }

    public boolean saveItemsAscync(final List<T> item) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(item);
            }
        });
        return true;
    }

    public boolean clearItems() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
            getAllItems().deleteAllFromRealm();
            }
        });
        return true;
    }

    public boolean removeItem(T t) {
        t.deleteFromRealm();
        return true;
    }

    public boolean clearItemsAscync() {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
            getAllItems().deleteAllFromRealm();
            }
        });
        return true;
    }
}

