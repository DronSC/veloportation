package com.kolibru.veloportation.database;

import io.realm.RealmObject;
import io.realm.RealmResults;


public interface InterfaceModel<T extends RealmObject> {

    RealmResults<T> getAllItems();
}

