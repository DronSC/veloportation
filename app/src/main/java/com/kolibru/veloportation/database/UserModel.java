package com.kolibru.veloportation.database;

import android.util.Log;

import com.kolibru.veloportation.models.User;

import io.realm.RealmResults;


public class UserModel extends BaseModel<User> {

    public UserModel(){
        super();
    }

    public boolean saveProfile(User profile) {
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(profile);
        }
        catch (Exception e){
            Log.e("saveCity","ERROR:"+(e.getMessage()==null?"[NULL]":e.getMessage()));
        }
        realm.commitTransaction();
        return false;
    }

    public User getUser(long id) {
        User res=realm
                .where(User.class)
                .equalTo("id",id)
                .findFirst();
        return res;
    }

    public void close() {
        realm.close();
    }

    @Override
    public RealmResults<User> getAllItems() {
        return realm
                .where(User.class)
                .findAll();
    }
}

