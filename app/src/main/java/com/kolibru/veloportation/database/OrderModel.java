package com.kolibru.veloportation.database;


import com.kolibru.veloportation.models.Order;

import java.util.List;

import io.realm.RealmResults;


public class OrderModel extends BaseModel<Order> {

    public OrderModel(){
        super();
    }

    public void saveRoutes(List<Order> routes) {
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(routes);
        }
        catch (Exception e){

        }
        realm.commitTransaction();
    }

    public Order getOrder(String id) {
        Order res=realm
                .where(Order.class)
                .equalTo("id", id)
                .findFirst();
        return res;
    }



    public RealmResults<Order> getAllItems(int state) {
        return realm
                .where(Order.class)
                .equalTo("state",state)
                .findAll();
    }

    @Override
    public RealmResults<Order> getAllItems() {
        return realm
                .where(Order.class)
                .findAll();
    }
}
