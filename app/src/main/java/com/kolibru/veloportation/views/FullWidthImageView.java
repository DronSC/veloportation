package com.kolibru.veloportation.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Andrej_Maligin on 15.12.2015.
 */
public class FullWidthImageView extends ImageView {
    public FullWidthImageView(Context context) {
        super(context);
    }

    public FullWidthImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()*9/16);
    }

}