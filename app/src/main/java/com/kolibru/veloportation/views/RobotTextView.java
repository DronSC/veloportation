package com.kolibru.veloportation.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Andrej_Maligin on 12.08.2015.
 */
public class RobotTextView extends TextView {

    private Typeface mTypeFace = null;

    public RobotTextView(Context context) {
        super(context);
        init();
    }

    public RobotTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RobotTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public RobotTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        if (mTypeFace == null) {
            mTypeFace = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
        }
        setTypeface(mTypeFace);

    }

}