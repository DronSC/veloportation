package com.kolibru.veloportation.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Andrej_Maligin on 12.08.2015.
 */
public class ExoLightTextView extends TextView {

    private Typeface mTypeFace = null;

    public ExoLightTextView(Context context) {
        super(context);
        init();
    }

    public ExoLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExoLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public ExoLightTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        if (mTypeFace == null) {
            mTypeFace = Typeface.createFromAsset(getContext().getAssets(), "fonts/Exo2-Light.otf");
        }
        setTypeface(mTypeFace);

    }

}