package com.kolibru.veloportation.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.FinanceClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.database.OrderModel;
import com.kolibru.veloportation.databinding.ActivityCreateOrderBinding;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.OnlineUsers;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.models.OrderPosition;
import com.kolibru.veloportation.models.PlacePosition;
import com.kolibru.veloportation.utils.KolibruDateFormatter;
import com.kolibru.veloportation.views.BottomDialog;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.joda.time.DateTime;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import noman.googleplaces.Place;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CreateOrder extends BaseActivity {

    ActivityCreateOrderBinding itemBinding;
    OrderModel orderModel;
    public final int REQUIRE_TO_MAP=223;
    public final int REQUIRE_FROM_MAP=224;
    public PlacePosition placePosition_from=null;
    public PlacePosition placePosition_to=null;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String endTime="";
    private DateTime selectTimeEnd=DateTime.now();
    private Timer mTimer = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_order);
        orderModel=new OrderModel();
        itemBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_order);
        itemBinding.addresFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(CreateOrder.this, SelectPointMap.class).putExtra("type_selector","from"),REQUIRE_FROM_MAP);
            }
        });
        itemBinding.addresTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(CreateOrder.this, SelectPointMap.class).putExtra("type_selector","to"),REQUIRE_TO_MAP);
            }
        });
        itemBinding.waitTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime(itemBinding.waitTime);
            }
        });
        itemBinding.cancelTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime="";
                itemBinding.waitTime.setText("");
            }
        });

    setSupportActionBar(itemBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.create_order));
        itemBinding.start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(placePosition_from!=null && placePosition_to!=null && itemBinding.phone.getText().length()>4) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Вы желаете создать новый заказ?")
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    webFunctionService.addOrder(
                                            itemBinding.phone.getText().toString(),
                                            itemBinding.message.getText().toString(),
                                            itemBinding.cost.getText().toString(),
                                            placePosition_from,
                                            placePosition_to,
                                            endTime,
                                            new Callback<BaseStruct<Order>>() {
                                                @Override
                                                public void success(BaseStruct<Order> orderBaseStruct, Response response) {
                                                    if (orderBaseStruct!=null && orderBaseStruct.getStatus() > 0) {
                                                        orderModel.saveItems(orderBaseStruct.getResult());
                                                        Intent intent = new Intent(getActivity(), OneOrderActivity.class);
                                                        Parcelable wrap = Parcels.wrap(Order.class, orderBaseStruct.getResult().get(0));
                                                        intent.putExtra(Order.class.getSimpleName(), wrap);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    Log.e("addOrder", "ERROR:" + ConstClass.responseToString(error.getResponse()));
                                                    Toast.makeText(getActivity(),getString(R.string.error_with_save),Toast.LENGTH_LONG).show();
                                                }
                                            }
                                    );
                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }
                else{
                    Toast.makeText(getActivity(),getString(R.string.not_set_fields),Toast.LENGTH_LONG).show();
                }
            }
        });
        itemBinding.online.setText(getString(R.string.text_online_user)+getString(R.string.online_user_not_found));
        startTimer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            orderModel.close();
            if (mTimer != null)
                mTimer.cancel();
        } catch (Exception e) {

        }
    }

    public void setTime(final TextView timer) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endTime=String.valueOf(year)+'-'+String.valueOf(monthOfYear)+'-'+String.valueOf(dayOfMonth);
                        selectTimeEnd=selectTimeEnd.withDate(year,monthOfYear+1,dayOfMonth);
                        mHour = c.get(Calendar.HOUR_OF_DAY);
                        mMinute = c.get(Calendar.MINUTE);

                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {

                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay,
                                                          int minute) {
                                        endTime+=" "+String.valueOf(hourOfDay)+":"+String.valueOf(minute);
                                        selectTimeEnd=selectTimeEnd.withTime(hourOfDay,minute,0,0);
                                        timer.setText(KolibruDateFormatter.getSimpleTextDateTime(selectTimeEnd));
                                    }
                                }, mHour, mMinute, false);
                        timePickerDialog.show();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch (requestCode){
                case REQUIRE_TO_MAP:{
                    if(resultCode==RESULT_OK){
                        placePosition_to = Parcels.unwrap(data.getParcelableExtra(PlacePosition.class.getSimpleName()));
                        if(placePosition_to!=null){
                            itemBinding.addresTo.setText(placePosition_to.getName());
                        }
                    }
                    break;
                }
                case REQUIRE_FROM_MAP:{
                    if(resultCode==RESULT_OK){
                        placePosition_from = Parcels.unwrap(data.getParcelableExtra(PlacePosition.class.getSimpleName()));
                        if(placePosition_from!=null){
                            itemBinding.addresFrom.setText(placePosition_from.getName());
                        }
                    }
                    break;
                }
            }
            itemBinding.distance.setText(String.valueOf(FinanceClass.getDistance(placePosition_from,placePosition_to))+" м");
            itemBinding.cost.setText(String.valueOf(FinanceClass.foundPrice(placePosition_from,placePosition_to)));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void startTimer() {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            public void run() {
                if(webFunctionService.isInternetOn(getActivity())) {
                    webFunctionService.getOnlineUsers(new Callback<BaseStruct<OnlineUsers>>() {
                        @Override
                        public void success(BaseStruct<OnlineUsers> onlineUsersBaseStruct, Response response) {
                            if (onlineUsersBaseStruct.getStatus() > 0 && onlineUsersBaseStruct.getResult().size()>0) {
                                itemBinding.online.setText(getString(R.string.text_online_user)+String.valueOf(onlineUsersBaseStruct.getResult().get(0).getCount_online()));
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("ERROR", ConstClass.responseToString(error.getResponse()));

                        }
                    });
                }
            }
        }, 0, 10000);
    }
}
