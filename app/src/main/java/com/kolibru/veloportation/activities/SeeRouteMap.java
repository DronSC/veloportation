package com.kolibru.veloportation.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.FinanceClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.databinding.ActivityFreeMapBinding;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.models.OrderPosition;
import com.kolibru.veloportation.service.OrderPositionService;
import com.kolibru.veloportation.utils.CurrentUserProfile;

import org.parceler.Parcels;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SeeRouteMap extends BaseMapFragmentActivity implements OnMapReadyCallback {

    private static final int REQUEST_CODE = 121;
    private LocationUpdateReceiver mLocationUpdateReceiver = new LocationUpdateReceiver();
    private ActivityFreeMapBinding mBinding;
    private Timer mTimer = null;
    private CurrentUserProfile currentUserProfile=null;
    Order order;
    private ServiceConnection sConn;
    private Intent serviceIntent;
    private final String LOG_TAG="asdf";

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mLocationUpdateReceiver, new IntentFilter(OrderPositionService.NEW_LOCATION_INFO));
    }


    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mLocationUpdateReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unbindService(sConn);
            stopService(serviceIntent);
            if(mTimer!=null)
                mTimer.cancel();
        } catch (Exception e) {

        }
    }


    private void drawPolyline() {
        /*List<LatLng> list = PolyUtil.decode(routeState.getRoute().getPolyline());
        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
        for (int z = 0; z < list.size(); z++) {
            LatLng point = list.get(z);
            options.add(point);
        }
        if (mMap != null)
            mMap.addPolyline(options);*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_map);
        currentUserProfile=CurrentUserProfile.getInstance(this);
        mBinding=DataBindingUtil.setContentView(this, R.layout.activity_free_map);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        order = Parcels.unwrap(getIntent().getParcelableExtra(Order.class.getSimpleName()));
        sConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d(LOG_TAG, "MainActivity onServiceConnected");
                //bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d(LOG_TAG, "MainActivity onServiceDisconnected");
                //bound = false;
            }
        };

        serviceIntent = new Intent(this, OrderPositionService.class);
        //serviceIntent.putExtra(PlacesService.ROUTE, Parcels.wrap(Route.class, route));
        startService(serviceIntent);
        bindService(serviceIntent,sConn,BIND_AUTO_CREATE);
        if(order!=null)
            mBinding.setItem(order);

        mBinding.client.setVisibility(View.VISIBLE);
        mBinding.courier.setVisibility(View.GONE);
        mBinding.distance.setText("~ "+FinanceClass.getDistance(order.getAddress_from(),order.getAddress_to())+" м");


        MapFragment mapFragment = new MapFragment();
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.map_container, mapFragment);
        transaction.commit();
        mapFragment.getMapAsync(this);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(SeeRouteMap.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SeeRouteMap.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startTimer();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
        }
        reloadMarker(order);
    }

    private class LocationUpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra("location");
        }
    }



    protected void startTimer() {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            public void run() {
                if(order==null) {
                    webFunctionService.getOrdersPosition(new Callback<BaseStruct<OrderPosition>>() {
                        @Override
                        public void success(BaseStruct<OrderPosition> orderPositionBaseStruct, Response response) {
                            if (orderPositionBaseStruct.getStatus() > 0) {
                                reloadMarker(orderPositionBaseStruct.getResult());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("ERROR", ConstClass.responseToString(error.getResponse()));

                        }
                    });
                }
                else {
                    webFunctionService.getOrdersPosition(order.getId(),new Callback<BaseStruct<OrderPosition>>() {
                        @Override
                        public void success(BaseStruct<OrderPosition> orderPositionBaseStruct, Response response) {
                            if (orderPositionBaseStruct.getStatus() > 0) {
                                reloadMarker(orderPositionBaseStruct.getResult());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("ERROR", ConstClass.responseToString(error.getResponse()));

                        }
                    });
                }
            }
        }, 0, 5000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
