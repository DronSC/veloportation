package com.kolibru.veloportation.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.plus.model.people.Person;
import com.kolibru.veloportation.*;
import com.kolibru.veloportation.database.UserModel;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.NotificationSettings;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.service.MyHandler;
import com.kolibru.veloportation.service.RegistrationIntentService;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.web.WebFunctionService;
import com.microsoft.windowsazure.notifications.NotificationsManager;

import java.lang.reflect.Type;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

public class Auth extends BaseActivity {

    WebFunctionService webFunctionService;
    UserModel userModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        webFunctionService=WebFunctionService.getInstance(this);
         userModel=new UserModel();
        (findViewById(R.id.reg)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Auth.this, Registration.class));
            }
        });

        (findViewById(R.id.auth)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login=((TextView)findViewById(R.id.login)).getText().toString();
                String pass=((TextView)findViewById(R.id.pass)).getText().toString();

                if(login.length()>0 && pass.length()>0 ){
                    appSetting.saveSharedPreferences(AppSetting.SETTING_LOGIN,login);
                    appSetting.saveSharedPreferences(AppSetting.SETTING_PASS,pass);
                    webFunctionService.auth(login, pass, new Callback<BaseStruct<User>>() {
                        @Override
                        public void success(BaseStruct<User> userBaseStruct, Response response) {
                            if(userBaseStruct.getStatus()==1) {
                                CurrentUserProfile currentUserProfile = CurrentUserProfile.getInstance(Auth.this);
                                if (currentUserProfile.loginUser(userBaseStruct, response)!=null) {
                                    currentUserProfile.loadUser(userBaseStruct.getResult().get(0));
                                    appSetting.saveSharedPreferences(AppSetting.SETTING_MY_PROFILE_ID,userBaseStruct.getResult().get(0).getId());
                                    startActivity(new Intent(Auth.this, MainUser.class));
                                    finish();
                                }
                                else{
                                    Toast.makeText(getApplicationContext(),userBaseStruct.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                Toast.makeText(getApplicationContext(),userBaseStruct.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("ERROR",ConstClass.responseToString(error.getResponse()));

                            appSetting.removeKey(AppSetting.SETTING_LOGIN);
                            appSetting.removeKey(AppSetting.SETTING_PASS);
                        }
                    });
                }
                else{
                    Toast.makeText(getApplicationContext(),getString(R.string.a_little_fields),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        userModel.close();
    }


}
