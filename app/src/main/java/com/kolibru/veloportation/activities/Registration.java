package com.kolibru.veloportation.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.tool.DataBinder;
import android.support.v4.content.pm.ActivityInfoCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.database.UserModel;
import com.kolibru.veloportation.databinding.ActivityRegistrationBinding;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.web.WebFunctionService;
import com.sevenheaven.segmentcontrol.SegmentControl;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

public class Registration extends BaseActivity {

    int type=1;
    ActivityRegistrationBinding itemBinding;
    WebFunctionService webFunctionService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        itemBinding= DataBindingUtil.setContentView(this, R.layout.activity_registration);
        webFunctionService=WebFunctionService.getInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //type=getIntent().getIntExtra("type",-1);
        itemBinding.segmentControl.setOnSegmentControlClickListener(new SegmentControl.OnSegmentControlClickListener() {
            @Override
            public void onSegmentControlClick(int index) {
            if(index==0){
                type=ConstClass.TypeUser.COURIER.getValue();
                Toast.makeText(getActivity(),"Курьер",Toast.LENGTH_SHORT).show();
            }
            else{
                type=ConstClass.TypeUser.CLIENT.getValue();
                Toast.makeText(getActivity(),"Клиент",Toast.LENGTH_SHORT).show();
            }
            }
        });
        itemBinding.butMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),ManualActivity.class));
            }
        });
        itemBinding.reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemBinding.checkBox.isChecked()) {
                    if (itemBinding.pass.getText().length() > 0 && itemBinding.pass.getText().toString().equals(itemBinding.repeatPass.getText().toString())) {
                        if (itemBinding.login.getText().length() > 0) {
                            String login = itemBinding.login.getText().toString();
                            String pass = itemBinding.pass.getText().toString();
                            String surname = itemBinding.surname.getText().toString();
                            String name = itemBinding.name.getText().toString();
                            String mobile = itemBinding.mobile.getText().toString();
                            if (login.length() > 0 && surname.length() > 0 && name.length() > 0 && mobile.length() > 0) {

                                appSetting.saveSharedPreferences(AppSetting.SETTING_LOGIN, login);
                                appSetting.saveSharedPreferences(AppSetting.SETTING_PASS, pass);
                                webFunctionService.registration(login, pass, surname, name, null, mobile, type, new Callback<BaseStruct<User>>() {
                                    @Override
                                    public void success(BaseStruct<User> userBaseStruct, Response response) {
                                        if (userBaseStruct.getStatus() == 1) {
                                            List<Header> headerList = response.getHeaders();
                                            for (Header header : headerList) {
                                                if (header.getName().equals("Set-Cookie")) {
                                                    String[] cookies = header.getValue().split(";");
                                                    for (String param : cookies) {
                                                        param = param.trim();
                                                        String[] key_val = param.split("=");
                                                        if (key_val[0].equals("veloport"))
                                                            webFunctionService.setTOKEN(key_val[1]);
                                                        break;
                                                    }
                                                }
                                            }
                                            CurrentUserProfile currentUserProfile = CurrentUserProfile.getInstance(Registration.this);
                                            UserModel userModel = new UserModel();
                                            userModel.saveItems(userBaseStruct.getResult());
                                            userModel.close();
                                            currentUserProfile.loadUser(userBaseStruct.getResult().get(0).getId());
                                            appSetting.saveSharedPreferences(AppSetting.SETTING_MY_PROFILE_ID, userBaseStruct.getResult().get(0).getId());
                                            startActivity(new Intent(Registration.this, MainUser.class));
                                        } else {
                                            Toast.makeText(getApplicationContext(), userBaseStruct.getMessage(), Toast.LENGTH_LONG).show();
                                        }

                                    }

                                    @Override
                                    public void failure(RetrofitError error) {

                                        appSetting.removeKey(AppSetting.SETTING_LOGIN);
                                        appSetting.removeKey(AppSetting.SETTING_PASS);
                                    }
                                });
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.a_little_person_info), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.a_little_login), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.a_little_pass), Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_no_confirm_rights), Toast.LENGTH_LONG).show();
                }
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
