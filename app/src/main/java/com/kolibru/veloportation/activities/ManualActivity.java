package com.kolibru.veloportation.activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kolibru.veloportation.R;
import com.kolibru.veloportation.databinding.FragmentManualBinding;

public class ManualActivity extends AppCompatActivity {

    FragmentManualBinding itemBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_manual);
        itemBinding = DataBindingUtil.setContentView(this, R.layout.fragment_manual);

        itemBinding.mWebView.getSettings().setJavaScriptEnabled(true);
        //itemBinding.mWebView.getSettings().setPluginsEnabled(true);
        itemBinding.mWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=http://velovod.com/rights.pdf");
    }


}
