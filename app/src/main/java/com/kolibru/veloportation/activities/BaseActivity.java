package com.kolibru.veloportation.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.web.WebFunctionService;
import com.vk.sdk.VKAccessToken;
import java.io.File;
import java.util.Date;

public class BaseActivity extends AppCompatActivity {

    protected BaseActivity getActivity(){
        return this;
    }
    protected WebFunctionService webFunctionService;
    protected AppSetting appSetting;
    protected String base64EncodedPublicKey =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjTfOYKgnBi1jcffYquaFkIEBSiiX/R3ltmDuktQyQ+TFiOmSGGn9ca6CkxDvf89MPzCPwj2UoPv7PN9O3kbsSCb+E5VxKkAbTDJdBV7JN0KGhhGAJK0iAXO9R8fdYCF7gw7uHxCPBg08wuZOQYKhIsxJ64dSY5LXe6I8CZNkk9Zp3ZCbILhsIjUy0fGTMWG3MoJU8qyknXKYkJKiid/tvum6Ljq7K3/Q+u8Vb/facHnneZVwotKO49vHzNR7UtbAyCoCZEFv6I4YPOKaWg8R6l0EZknVBLR7zmE8kwjRMSCb2LXHumLrzWZR1/X1qo5ZypUaETy4aac7YN0gKQq80wIDAQAB";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webFunctionService=WebFunctionService.getInstance(this);
        appSetting=AppSetting.getInstance(this);
    }

    protected String getLogName(){
        return getLogName("");
    }

    protected String getLogName(String append){
        try {
            return this.getClass().getName()+"_"+append;
        }
        catch (Exception e){
            return "BaseActivity";
        }
    }

    protected  boolean isTurnGPS(){
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                Toast.makeText(this,getString(R.string.error_no_right_gps),Toast.LENGTH_LONG).show();
                return false;
            }
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                return true;
            }
            return false;
        }
        catch (Exception e){
            return false;
        }
    }

    protected void myFinish(int res) {
        setResult(res,new Intent());
        finish();
    }

    protected void myFinish(int res,Intent intent) {
        setResult(res,intent);
        finish();
    }

    public void setTranslucentStatusBar(Window window) {
        if (window == null) return;
        int sdkInt = Build.VERSION.SDK_INT;
        if (sdkInt >= Build.VERSION_CODES.LOLLIPOP) {
            setTranslucentStatusBarLollipop(window);
        } else if (sdkInt >= Build.VERSION_CODES.KITKAT) {
            setTranslucentStatusBarKiKat(window);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void setTranslucentStatusBarLollipop(Window window) {
        window.setStatusBarColor(
                ConstClass.getColor(getApplicationContext(),R.color.trans));
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected static void setTranslucentStatusBarKiKat(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public boolean haveInternet(){
        return webFunctionService.isInternetOn(this);
    }

    /**
     * Перезагружаем активити
     */
    public void reload() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }



    public StaggeredGridLayoutManager getGridLaytoutManager(int count) {
        return new StaggeredGridLayoutManager(count, StaggeredGridLayoutManager.VERTICAL);
    }


    protected Bitmap takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            return bitmap;
           /* File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();*/

            //openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
        return null;
    }


    int getMyId() {
        final VKAccessToken vkAccessToken = VKAccessToken.currentToken();
        return vkAccessToken != null ? Integer.parseInt(vkAccessToken.userId) : 0;
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }


}
