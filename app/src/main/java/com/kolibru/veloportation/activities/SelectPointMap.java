package com.kolibru.veloportation.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.databinding.ActivitySelectPointMapBinding;
import com.kolibru.veloportation.models.GooglePositionInfo;
import com.kolibru.veloportation.models.GoogleSearchSuggession;
import com.kolibru.veloportation.models.PlacePosition;
import com.kolibru.veloportation.models.StructGooglePosition;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.web.WebGoogleFunctionService;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SelectPointMap extends BaseMapFragmentActivity implements OnMapReadyCallback,LocationListener {

    private ActivitySelectPointMapBinding mBinding;
    private CurrentUserProfile currentUserProfile=null;
    private WebGoogleFunctionService webGoogleFunctionService;
    protected  List<GoogleSearchSuggession> places=new ArrayList<>();
    protected LatLng selectPosition=null;
    protected String selectTitle=null;
    protected Location currentUserLocation=null;


    protected View.OnClickListener selectButton=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectPosition!=null){
                PlacePosition placePosition=new PlacePosition();
                placePosition.setName(selectTitle);
                placePosition.setLatitude(selectPosition.latitude);
                placePosition.setLongitude(selectPosition.longitude);
                Intent intent=new Intent();
                Parcelable wrap = Parcels.wrap(PlacePosition.class, placePosition);
                intent.putExtra(PlacePosition.class.getSimpleName(), wrap);
                myFinish(RESULT_OK,intent);
            }
            else {
                myFinish(RESULT_CANCELED);
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
       // if (mGoogleApiClient != null) {
       //     LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
       // }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_point_map);
        mBinding=DataBindingUtil.setContentView(this, R.layout.activity_select_point_map);
        webGoogleFunctionService=new WebGoogleFunctionService();
        currentUserProfile=CurrentUserProfile.getInstance(this);

        MapFragment mapFragment = new MapFragment();
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.map_container, mapFragment);
        transaction.commit();
        mapFragment.getMapAsync(this);

        mBinding.selectChoice.setEnabled(false);
        //mBinding.myPosition.setEnabled(false);
       // mBinding.myPosition.setOnClickListener(selectButton);
        mBinding.selectChoice.setOnClickListener(selectButton);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getIntent()!=null ){
            String tmp=getIntent().getStringExtra("type_selector");
            if(tmp!=null && tmp.equals("from")){
                setTitle(getString(R.string.select_send_from));
                mBinding.selectChoice.setText(getString(R.string.select_send_from));
            }
            else{
                setTitle(getString(R.string.select_send_to));
                mBinding.selectChoice.setText(getString(R.string.select_send_to));
            }
        }
        mBinding.myPosition.setEnabled(false);
        mBinding.searchbox.setSearchHint("поиск адреса...");
        mBinding.searchbox.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                GoogleSearchSuggession mySearchSuggestion=(GoogleSearchSuggession)searchSuggestion;
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mySearchSuggestion.getLatitude(), mySearchSuggestion.getLongitude()))
                        .title(mySearchSuggestion.getBody())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mySearchSuggestion.getLatitude(), mySearchSuggestion.getLongitude()), 15));
                selectPosition=new LatLng(mySearchSuggestion.getLatitude(), mySearchSuggestion.getLongitude());
                selectTitle=mySearchSuggestion.getBody();
                mBinding.selectChoice.setEnabled(true);

                mBinding.address.setText(selectTitle);
                mBinding.address.setTextColor(ConstClass.getColor(getActivity(),R.color.colorPrimary));
            }

            @Override
            public void onSearchAction(String currentQuery) {

            }
        });

        mBinding.searchbox.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {

            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                if(newQuery.length()>4) {
                    webGoogleFunctionService.getLocationByAddress(newQuery, new Callback<StructGooglePosition>() {
                        @Override
                        public void success(final StructGooglePosition structGooglePosition, Response response) {
                            //  try {
                            if (response != null) {

                                places = new ArrayList<GoogleSearchSuggession>();
                                for (GooglePositionInfo googlePositionInfo : structGooglePosition.getResult()) {
                                    GoogleSearchSuggession googleSearchSuggession = new GoogleSearchSuggession(
                                            googlePositionInfo.getFormatted_address(),
                                            new LatLng(googlePositionInfo.getGeometry().getLocation().getLat(), googlePositionInfo.getGeometry().getLocation().getLng()));
                                    places.add(googleSearchSuggession);
                                }
                                if (places.size() > 0)
                                    mBinding.searchbox.swapSuggestions(places);
                            }
                            //  }
                            //  catch (Exception e){
                            //
                            //  }
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
                }
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(SelectPointMap.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SelectPointMap.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationManager locationManager =
                (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);
        currentUserLocation = locationManager.getLastKnownLocation(provider);
        locationManager.requestLocationUpdates(provider, 2000, 1,  this);

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false); // delete default button

        mBinding.myPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentUserLocation!=null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new
                            LatLng(currentUserLocation.getLatitude(),
                            currentUserLocation.getLongitude()), 15));
                }
            }
        });
        mMap.setMyLocationEnabled(true);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(final LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(getString(R.string.your_choice))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))

                );
                selectPosition=latLng;
                mBinding.selectChoice.setEnabled(false);
                mBinding.address.setText(getString(R.string.find_place));
                mBinding.address.setTextColor(ConstClass.getColor(getActivity(),R.color.base_line_color));
                webGoogleFunctionService.getLocationByLatLAng(latLng.latitude, latLng.longitude, new Callback<StructGooglePosition>() {

                    @Override
                    public void success(StructGooglePosition structGooglePosition, Response response) {
                        try {
                            if (response != null) {
                                for (GooglePositionInfo googlePositionInfo : structGooglePosition.getResult()) {
                                    mMap.clear();
                                    mMap.addMarker(new MarkerOptions()
                                            .position(latLng)
                                            .title(googlePositionInfo.getFormatted_address())
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                    );
                                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                                    selectTitle=googlePositionInfo.getFormatted_address();
                                    mBinding.selectChoice.setEnabled(true);

                                    mBinding.address.setText(selectTitle);
                                    mBinding.address.setTextColor(ConstClass.getColor(getActivity(),R.color.colorPrimary));
                                    break;
                                }
                            }
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("getMyPositionInfo", "ERROR:" + (error.getMessage() == null ? "[NULL]" : error.getMessage()));

                    }
                });
            }
        });
    }








    private void centerMapOnNextPoint(Location location) {
        try {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            //CameraPosition cameraUpdate = new CameraPosition(latLng, 17f, 1f, 1f);
            // mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraUpdate));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            mMap.animateCamera(cameraUpdate);
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLocationChanged(Location location) {
        currentUserLocation=location;
        mBinding.myPosition.setEnabled(true);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
