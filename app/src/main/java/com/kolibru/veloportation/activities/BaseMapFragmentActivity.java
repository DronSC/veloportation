package com.kolibru.veloportation.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.models.MarkerForOrder;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.models.OrderPosition;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.web.WebFunctionService;
import com.vk.sdk.VKAccessToken;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BaseMapFragmentActivity extends BaseActivity {

    protected GoogleMap mMap;
    private boolean isSet=false;
    private HashMap<Order,MarkerForOrder> markerForOrderList=new HashMap<>();
    protected int ZOOM_CITY=12;
    protected int ZOOM_PEOPLE=15;
    protected int ZOOM_PLACE=14;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                //TODO:
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                //(just doing it here for now, note that with this code, no explanation is shown)
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    protected void reloadMarker(List<OrderPosition> list) {
        //mMap.clear();
        for (OrderPosition orderPosition : list) {
            MarkerForOrder markerForOrder;
            if(!markerForOrderList.containsKey(orderPosition)){
                markerForOrder=new MarkerForOrder();
                markerForOrderList.put(orderPosition.getOrder(),markerForOrder);
            }
            else{
                markerForOrder=markerForOrderList.get(orderPosition);
            }
            if(orderPosition.getPosition()!=null) {
                LatLng pos = new LatLng(orderPosition.getPosition().getLat(), orderPosition.getPosition().getLon());
                if(markerForOrder.getCourier()==null) {
                    markerForOrder.setCourier(new MarkerOptions()
                            .position(pos)
                            .title("Курьер: " + orderPosition.getCourier().getFio())
                            .snippet("Заказ: " + orderPosition.getOrder().getMessage())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                    mMap.addMarker(markerForOrder.getCourier());
                }
                else{
                    markerForOrder.getCourier().position(pos);
                }
                if(!isSet) {
                    centerMapOnLocation(pos);
                    isSet=true;
                }
            }
            if(orderPosition.getOrder()!=null) {
                if(orderPosition.getOrder().getAddress_from()!=null) {
                    LatLng pos = new LatLng(orderPosition.getOrder().getAddress_from().getLatitude(), orderPosition.getOrder().getAddress_from().getLongitude());
                    if(markerForOrder.getMarker_from()==null) {
                        markerForOrder.setMarker_from(new MarkerOptions()
                                .position(pos)
                                .title("Заказ №" + String.valueOf(orderPosition.getOrder().getId()) + " | " + getString(R.string.send_from))
                                .snippet(orderPosition.getOrder().getAddress_from().getName())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        mMap.addMarker(markerForOrder.getMarker_from());
                    }
                    else{
                        markerForOrder.getMarker_from().position(pos);

                    }
                    if(!isSet) {
                        centerMapOnLocation(pos);
                        isSet=true;
                    }
                }
                if(orderPosition.getOrder().getAddress_to()!=null) {
                    LatLng pos = new LatLng(orderPosition.getOrder().getAddress_to().getLatitude(), orderPosition.getOrder().getAddress_to().getLongitude());
                    if(markerForOrder.getMarker_to()==null) {
                        markerForOrder.setMarker_to(new MarkerOptions()
                                .position(pos)
                                .title("Заказ №" + String.valueOf(orderPosition.getOrder().getId()) + " | " + getString(R.string.send_to))
                                .snippet(orderPosition.getOrder().getAddress_to().getName())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        mMap.addMarker(markerForOrder.getMarker_to());
                    }
                    else {
                        markerForOrder.getMarker_to().position(pos);
                    }
                    if(!isSet) {
                        centerMapOnLocation(pos);
                        isSet=true;
                    }
                }
            }
        }
    }

    protected void reloadMarker(Order orderPosition) {
        MarkerForOrder markerForOrder;
        if(!markerForOrderList.containsKey(orderPosition)){
            markerForOrder=new MarkerForOrder();
            markerForOrderList.put(orderPosition,markerForOrder);
        }
        else{
            markerForOrder=markerForOrderList.get(orderPosition);
        }
        if(orderPosition.getAddress_from()!=null) {
            LatLng pos = new LatLng(orderPosition.getAddress_from().getLatitude(), orderPosition.getAddress_from().getLongitude());
            if(markerForOrder.getMarker_from()==null) {
                markerForOrder.setMarker_from(new MarkerOptions()
                        .position(pos)
                        .title("Заказ №" + String.valueOf(orderPosition.getId()) + " | " + getString(R.string.send_from))
                        .snippet(orderPosition.getAddress_from().getName())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                mMap.addMarker(markerForOrder.getMarker_from());
            }
            else{
                markerForOrder.getMarker_from().position(pos);

            }
            if(!isSet) {
                centerMapOnLocation(pos);
                isSet=true;
            }
        }
        if(orderPosition.getAddress_to()!=null) {
            LatLng pos = new LatLng(orderPosition.getAddress_to().getLatitude(), orderPosition.getAddress_to().getLongitude());
            if(markerForOrder.getMarker_to()==null) {
                markerForOrder.setMarker_to(new MarkerOptions()
                        .position(pos)
                        .title("Заказ №" + String.valueOf(orderPosition.getId()) + " | " + getString(R.string.send_to))
                        .snippet(orderPosition.getAddress_to().getName())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                mMap.addMarker(markerForOrder.getMarker_to());
            }
            else {
                markerForOrder.getMarker_to().position(pos);
            }
            if(!isSet) {
                centerMapOnLocation(pos);
                isSet=true;
            }
        }
    }

    private void centerMapOnLocation(Location location) {
        try {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_PLACE);
            mMap.animateCamera(cameraUpdate);
        } catch (Exception e) {
        }
    }

    private void centerMapOnLocation(LatLng latLng) {
        try {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_PLACE);
            mMap.animateCamera(cameraUpdate);
        } catch (Exception e) {
        }
    }
}
