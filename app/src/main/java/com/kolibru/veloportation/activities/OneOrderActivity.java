package com.kolibru.veloportation.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.database.OrderModel;
import com.kolibru.veloportation.databinding.ActivityOneOrderBinding;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.utils.KolibruDateFormatter;
import com.kolibru.veloportation.views.BottomDialog;
import com.kolibru.veloportation.web.WebFunctionService;
import com.mypopsy.maps.StaticMap;

import org.joda.time.DateTime;
import org.parceler.Parcels;

import java.net.MalformedURLException;
import java.util.Calendar;

import nl.changer.audiowife.AudioWife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.graphics.Color.BLUE;

public class OneOrderActivity extends BaseActivity {

    private ActivityOneOrderBinding itemBinding;
    private Order order;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String endTime = null;
    private DateTime selectTimeEnd = DateTime.now();
    private CurrentUserProfile currentUserProfile;

    @Override
    public void onDestroy() {
        super.onDestroy();
        AudioWife.getInstance().release();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_order);
        currentUserProfile = CurrentUserProfile.getInstance(this);
        order = Parcels.unwrap(getIntent().getParcelableExtra(Order.class.getSimpleName()));
        itemBinding = DataBindingUtil.setContentView(this, R.layout.activity_one_order);
        setSupportActionBar(itemBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        itemBinding.phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + itemBinding.tel.getText().toString()));
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    Toast.makeText(getActivity(),"Вам необходимо дать разрешение, на совершение звонков из приложения!", Toast.LENGTH_LONG).show();
                    return;
                }
                startActivity(intent);
            }
        });
        itemBinding.route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OneOrderActivity.this, SeeRouteMap.class);
                Parcelable wrap = Parcels.wrap(Order.class, order);
                intent.putExtra(Order.class.getSimpleName(), wrap);
                startActivity(intent);
            }
        });

        itemBinding.butOpinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageOpinion();
            }
        });

        refreshOrder(order);
    }

    private void refreshOrder(final Order order){
        itemBinding.setItem(order);
        switch (CurrentUserProfile.type){
            case 1:{
                itemBinding.sr.setVisibility(View.VISIBLE);
                itemBinding.sr.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showMessageIDo();
                    }
                });
                itemBinding.butCancel.setVisibility(View.GONE);
                break;
            }
            case 2:{
                itemBinding.sr.setVisibility(View.GONE);

                try {
                    if (order.getCreate_user() == appSetting.getLong(AppSetting.SETTING_MY_PROFILE_ID)) {
                        itemBinding.butCancel.setVisibility(View.VISIBLE);
                        itemBinding.butCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(getString(R.string.question_cancel_oreder))
                                .setCancelable(false)
                                .setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    webFunctionService.cancelOrder(
                                            order.getId(),
                                            new Callback<BaseStruct<Order>>() {
                                                @Override
                                                public void success(BaseStruct<Order> orderBaseStruct, Response response) {
                                                    try {
                                                        if (orderBaseStruct.getStatus() == 1) {
                                                            if (orderBaseStruct.getResult().size() > 0) {
                                                                refreshOrder(orderBaseStruct.getResult().get(0));
                                                            }
                                                        }
                                                    }
                                                    catch (Exception e){

                                                    }
                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    Log.e("addOrder", "ERROR:" + ConstClass.responseToString(error.getResponse()));
                                                   // Toast.makeText(getActivity(),getString(R.string.error_with_save),Toast.LENGTH_LONG).show();
                                                }
                                            }
                                    );
                                        dialog.cancel();
                                    }
                                })
                                .setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                            AlertDialog alert = builder.create();
                            alert.show();
                            }
                        });
                    }
                }
                catch (Exception e){

                }
                break;
            }
            default:{
                itemBinding.sr.setVisibility(View.GONE);
                itemBinding.courierBlock.setVisibility(View.GONE);
                break;
            }
        }
        switch (order.getState()) {
            case 0:{
                itemBinding.state.setText(getActivity().getString(R.string.state_free));
                itemBinding.state.setTextColor(ConstClass.getColor(getActivity(),R.color.primary));
                itemBinding.butOpinion.setVisibility(View.GONE);
                break;
            }
            case 1:{
                itemBinding.state.setText(getActivity().getString(R.string.state_start));
                itemBinding.state.setTextColor(ConstClass.getColor(getActivity(),R.color.yellow));
                itemBinding.sr.setVisibility(View.GONE);
                itemBinding.butOpinion.setVisibility(View.GONE);
                break;
            }
            case 2:{
                itemBinding.state.setText(getActivity().getString(R.string.state_finish));
                itemBinding.state.setTextColor(ConstClass.getColor(getActivity(),R.color.green));
                itemBinding.sr.setVisibility(View.GONE);
                itemBinding.butCancel.setVisibility(View.GONE);
                if(currentUserProfile.type==2)
                    itemBinding.butOpinion.setVisibility(View.VISIBLE);
                itemBinding.gotomap.setVisibility(View.GONE);
                break;
            }
            default:{
                itemBinding.state.setText(getActivity().getString(R.string.state_canceled));
                itemBinding.state.setTextColor(ConstClass.getColor(getActivity(),R.color.accent_gray));
                itemBinding.sr.setVisibility(View.GONE);
                itemBinding.gotomap.setVisibility(View.GONE);
                itemBinding.butCancel.setVisibility(View.GONE);
                itemBinding.butOpinion.setVisibility(View.GONE);
                itemBinding.gotomap.setVisibility(View.GONE);
                break;
            }
        }

        if(order.getTake_order()!=null) {
            itemBinding.timeEnd.setText(KolibruDateFormatter.getSimpleDateWithMontheText(order.getTake_order().getEnd_time()));
            itemBinding.courierMessage.setText(order.getTake_order().getMessage());
            itemBinding.courierBlock.setVisibility(View.VISIBLE);
            itemBinding.courierFio.setText(order.getTake_order().getTake_fio());
            if(order.getTake_order().getTake_rating()==null)
                itemBinding.courierRating.setText("<не выставлялся>");
            else
                itemBinding.courierRating.setText(order.getTake_order().getTake_rating());
            try {
                if (order.getTake_order().getCreate_user() == appSetting.getLong(AppSetting.SETTING_MY_PROFILE_ID)
                        || order.getCreate_user() == appSetting.getLong(AppSetting.SETTING_MY_PROFILE_ID)) {
                    itemBinding.gotomap.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(OneOrderActivity.this, FreeMap.class);
                            Parcelable wrap = Parcels.wrap(Order.class, order);
                            intent.putExtra(Order.class.getSimpleName(), wrap);
                            startActivity(intent);
                        }
                    });
                }
                else {
                    itemBinding.gotomap.setVisibility(View.GONE);
                }
            }
            catch (Exception e){

            }
        }
    }



    public void setTime(final TextView timer) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endTime=String.valueOf(year)+'-'+String.valueOf(monthOfYear+1)+'-'+String.valueOf(dayOfMonth);
                        selectTimeEnd=selectTimeEnd.withDate(year,monthOfYear+1,dayOfMonth);
                        mHour = c.get(Calendar.HOUR_OF_DAY);
                        mMinute = c.get(Calendar.MINUTE);

                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {

                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay,
                                                          int minute) {
                                        endTime+=" "+String.valueOf(hourOfDay)+":"+String.valueOf(minute);
                                        selectTimeEnd=selectTimeEnd.withTime(hourOfDay,minute,0,0);
                                        timer.setText(KolibruDateFormatter.getSimpleTextDateTime(selectTimeEnd));
                                    }
                                }, mHour, mMinute, false);
                        timePickerDialog.show();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    public void showMessageIDo() {
        final LayoutInflater factory = getActivity().getLayoutInflater();
        final View view = factory.inflate(R.layout.message_i_do, null);
        final EditText message=(EditText)view.findViewById(R.id.value);
        final ImageView map_img=(ImageView)view.findViewById(R.id.map_container);
        final TextView timer=(TextView)view.findViewById(R.id.time_end);
        final ImageButton time_end=(ImageButton)view.findViewById(R.id.btn_time);
        time_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime(timer);
            }
        });

        StaticMap map=new StaticMap()
                .size(480, 480)
                .marker(StaticMap.Marker.Style.RED, new StaticMap.GeoPoint(order.getAddress_from().getLatitude(),order.getAddress_from().getLongitude()))
                .marker(StaticMap.Marker.Style.RED, new StaticMap.GeoPoint(order.getAddress_to().getLatitude(),order.getAddress_to().getLongitude())) ;

        try {
            Glide.with(getActivity())
                    .load(map.toURL().toString())
                    .into(map_img); // load into your image view
        }
        catch (Exception e){

        }
        new BottomDialog.Builder(getActivity())
            .setTitle(getString(R.string.text_order_agree))
            .setCustomView(view)
            .setNegativeText(getString(R.string.button_text_cancel))
            .onNegative(new BottomDialog.ButtonCallback() {
                @Override
                public void onClick(@NonNull BottomDialog dialog) {
                    dialog.dismiss();
                }
            })
            .setPositiveText(getString(R.string.text_agree))
            .onPositive(new BottomDialog.ButtonCallback() {
                @Override
                public void onClick(final @NonNull BottomDialog dialog) {
                webFunctionService.takeOrder(
                    order.getId(),
                    message.getText().toString(),
                    endTime,
                    1,
                    new Callback<BaseStruct<Order>>() {
                        @Override
                        public void success(BaseStruct<Order> orderBaseStruct, Response response) {
                            if (orderBaseStruct!=null && orderBaseStruct.getStatus() > 0) {
                                Intent intent=new Intent(OneOrderActivity.this,FreeMap.class);
                                Parcelable wrap = Parcels.wrap(Order.class, order);
                                intent.putExtra(Order.class.getSimpleName(), wrap);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("takeOrder", "ERROR:" + ConstClass.responseToString(error.getResponse()));
                            Toast.makeText(getActivity(),getString(R.string.error_with_save),Toast.LENGTH_LONG).show();
                        }
                    }
                );
                }
            })
            .show();
    }

    public void showMessageOpinion() {
        final LayoutInflater factory = getActivity().getLayoutInflater();
        final View view = factory.inflate(R.layout.message_opinion, null);
        final EditText message=(EditText)view.findViewById(R.id.message);
        final RatingBar stars=(RatingBar)view.findViewById(R.id.stars);
        stars.setMax(5);
        stars.setNumStars(5);
        stars.setRating(5);
        //stars.setAnimation(new RatingAnimation(stars));
        new BottomDialog.Builder(getActivity())
                .setTitle(getString(R.string.button_opinion))
                .setCustomView(view)
                .setNegativeText(getString(R.string.button_text_cancel))
                .onNegative(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull BottomDialog dialog) {
                        dialog.dismiss();
                    }
                })
                .setPositiveText(getString(R.string.text_agree))
                .onPositive(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(final @NonNull BottomDialog dialog) {
                        try {
                            webFunctionService.addOpinion(order.getTake_order().getCreate_user(), message.getText().toString(), String.valueOf(stars.getRating()), new Callback<BaseStruct<User>>() {
                                @Override
                                public void success(BaseStruct<User> userBaseStruct, Response response) {
                                    if(userBaseStruct.getStatus()>0) {
                                        downloadOrder();
                                    }
                                    else{
                                        Toast.makeText(getActivity(),userBaseStruct.getMessage(),Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {

                                    Log.e("addOrder", "ERROR:" + ConstClass.responseToString(error.getResponse()));
                                }
                            });
                        }
                        catch (Exception e){

                        }
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }


    private void downloadOrder(){
        webFunctionService.getOrder(order.getId(), new Callback<BaseStruct<Order>>() {
            @Override
            public void success(BaseStruct<Order> orderBaseStruct, Response response) {
                try {
                    if (orderBaseStruct.getStatus() == 1) {
                        order=orderBaseStruct.getResult().get(0);
                        if (orderBaseStruct.getResult().size() > 0) {
                            refreshOrder(orderBaseStruct.getResult().get(0));
                        }
                    }
                }
                catch (Exception e){

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Snackbar
                        .make(itemBinding.getRoot(),getString(R.string.error_with_connect),Snackbar.LENGTH_LONG)
                        .setActionTextColor(ConstClass.getColor(getActivity(),R.color.white))
                        .show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        if (id == R.id.refresh) {
            if(WebFunctionService.isInternetOn(getActivity())) {
                downloadOrder();
            }
            else {
                Snackbar
                    .make(itemBinding.getRoot(),getString(R.string.error_no_internet),Snackbar.LENGTH_LONG)
                    .setActionTextColor(ConstClass.getColor(getActivity(),R.color.white))
                    .show();
            }
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private class RatingAnimation extends Animation {

        RatingBar ratingBar;
        public RatingAnimation(RatingBar ratingBar) {
            this.ratingBar=ratingBar;
            setDuration(ratingBar.getNumStars()
                    * 4 * getResources().getInteger(android.R.integer.config_longAnimTime));
            setInterpolator(new LinearInterpolator());
            setRepeatCount(Animation.INFINITE);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            int progress = Math.round(interpolatedTime *ratingBar.getMax());
            ratingBar.setProgress(progress);
        }

        @Override
        public boolean willChangeTransformationMatrix() {
            return false;
        }

        @Override
        public boolean willChangeBounds() {
            return false;
        }
    }
}
