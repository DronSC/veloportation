package com.kolibru.veloportation.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.database.OrderModel;
import com.kolibru.veloportation.databinding.ActivityMainUserBinding;
import com.kolibru.veloportation.fragments.AboutFragment;
import com.kolibru.veloportation.fragments.ManualFragment;
import com.kolibru.veloportation.fragments.MyProfileFragment;
import com.kolibru.veloportation.fragments.OrdersFragment;
import com.kolibru.veloportation.fragments.SettingFragment;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.NotificationSettings;
import com.kolibru.veloportation.models.Order;
import com.kolibru.veloportation.models.User;
import com.kolibru.veloportation.service.MyHandler;
import com.kolibru.veloportation.service.RegistrationIntentService;
import com.kolibru.veloportation.utils.AppSetting;
import com.kolibru.veloportation.utils.CurrentUserProfile;
import com.kolibru.veloportation.web.WebFunctionService;
import com.microsoft.windowsazure.notifications.NotificationsManager;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainUser extends BaseActivity {

    public static final int REQUEST_PROFILE = 20160;

    private static final String TAG = "MainUser";
    public static MainUser mainActivity;

    private static final int REQUEST_EXIT = -1991;
    private CurrentUserProfile currentUserProfile;
    private ActivityMainUserBinding activityMainBinding;
    private ActionBarDrawerToggle drawerToggle = null;
    public static Boolean isVisible = false;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    Drawer result;

    public void registerWithNotificationHubs()
    {
        if (checkPlayServices()) {
            // Start IntentService to register this application with FCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported by Google Play Services.");
                ToastNotify("This device is not supported by Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user);
        mainActivity = this;
        currentUserProfile = CurrentUserProfile.getInstance(getActivity());
        NotificationsManager.handleNotifications(this, NotificationSettings.SenderId, MyHandler.class);
        registerWithNotificationHubs();
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_user);
        new DrawerBuilder().withActivity(this).build();
        initView();
        setSupportActionBar(activityMainBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        goToFragment(new OrdersFragment());
    }

    /**
     * Переход на новый фрагмент
     * @param f_new
     */
    public void goToFragment(Fragment f_new) {
        String TAG = f_new.getClass().getSimpleName();

        getSupportFragmentManager().popBackStackImmediate();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frag_main, f_new, TAG)
                //.addToBackStack(null)
                .commit();
    }



    /**
     * Инифиализация элементов активити
     */
    private void initView() {

        ProfileDrawerItem account;
        if(currentUserProfile.ava>0) {
            account=new ProfileDrawerItem().withName(currentUserProfile.getFio()).withEmail(currentUserProfile.mobile).withIcon(currentUserProfile.getImageLink());
        }
        else {
            account=new ProfileDrawerItem().withName(currentUserProfile.getFio()).withEmail(currentUserProfile.mobile).withIcon(R.drawable.user);
        }

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.pic_header)
                .addProfiles(
                        account
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(getString(R.string.question_need_login))
                                .setCancelable(false)
                                .setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if(WebFunctionService.isInternetOn(getActivity())) {
                                            webFunctionService.logout(new Callback<BaseStruct<User>>() {
                                                @Override
                                                public void success(BaseStruct<User> userBaseStruct, Response response) {
                                                    currentUserProfile.logoutUser();
                                                    startActivity(new Intent(getActivity(), Auth.class));
                                                    getActivity().finish();
                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    Snackbar
                                                        .make(activityMainBinding.getRoot(),getString(R.string.error_with_connect),Snackbar.LENGTH_LONG)
                                                        .setActionTextColor(ConstClass.getColor(getActivity(),R.color.white))
                                                        .show();

                                                    currentUserProfile.logoutUser();
                                                    startActivity(new Intent(getActivity(), Auth.class));
                                                    getActivity().finish();
                                                }
                                            });
                                        }
                                        else {
                                            Snackbar
                                                .make(activityMainBinding.getRoot(),getString(R.string.error_no_internet),Snackbar.LENGTH_LONG)
                                                .setActionTextColor(ConstClass.getColor(getActivity(),R.color.white))
                                                .show();
                                        }
                                        dialog.cancel();
                                    }
                                })
                                .setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                        return false;
                    }
                })
                .withSelectionListEnabledForSingleProfile(false)
                .build();
        if(currentUserProfile.type==1) {
            result = new DrawerBuilder()
                    .withActivity(this)
                    .withToolbar(activityMainBinding.toolbar)
                    .addDrawerItems(
                        /*new PrimaryDrawerItem()
                                .withIdentifier(1)
                                .withName(R.string.menu_text_profile)
                                .withIcon(R.drawable.ic_profile)
                                .withIconTintingEnabled(true)
                                .withIconColor(ConstClass.getColor(getActivity(),R.color.base_label_color))
                                .withTextColor(ConstClass.getColor(getActivity(),R.color.base_label_color))
                                .withSelectedTextColor(ConstClass.getColor(getActivity(),R.color.base_value_color))
                                .withSelectedIconColor(ConstClass.getColor(getActivity(),R.color.base_value_color)),*/
                            new PrimaryDrawerItem()
                                    .withIdentifier(2)
                                    .withName(R.string.menu_text_orders)
                                    .withIcon(R.drawable.ic_case)
                                    .withIconTintingEnabled(true)
                                    .withIconColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withTextColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withSelectedTextColor(ConstClass.getColor(getActivity(), R.color.base_value_color))
                                    .withSelectedIconColor(ConstClass.getColor(getActivity(), R.color.base_value_color)),
                            new PrimaryDrawerItem()
                                    .withIdentifier(3)
                                    .withName(R.string.menu_text_manual)
                                    .withIcon(R.drawable.ic_alert)
                                    .withIconTintingEnabled(true)
                                    .withIconColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withTextColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withSelectedTextColor(ConstClass.getColor(getActivity(), R.color.base_value_color))
                                    .withSelectedIconColor(ConstClass.getColor(getActivity(), R.color.base_value_color)),
                            new PrimaryDrawerItem()
                                    .withIdentifier(5)
                                    .withName(R.string.menu_text_setting)
                                    .withIcon(R.drawable.ic_setting)
                                    .withIconTintingEnabled(true)
                                    .withIconColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withTextColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withSelectedTextColor(ConstClass.getColor(getActivity(), R.color.base_value_color))
                                    .withSelectedIconColor(ConstClass.getColor(getActivity(), R.color.base_value_color))
                        /*new PrimaryDrawerItem()
                                .withIdentifier(4)
                                .withName(R.string.menu_text_about)
                                .withIcon(R.drawable.ic_information)
                                .withIconTintingEnabled(true)
                                .withIconColor(ConstClass.getColor(getActivity(),R.color.base_label_color))
                                .withTextColor(ConstClass.getColor(getActivity(),R.color.base_label_color))
                                .withSelectedTextColor(ConstClass.getColor(getActivity(),R.color.base_value_color))
                                .withSelectedIconColor(ConstClass.getColor(getActivity(),R.color.base_value_color))*/
                    )
                    .withActionBarDrawerToggle(true)
                    .withAccountHeader(headerResult)
                    .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                            switch ((int) drawerItem.getIdentifier()) {
                                case (1): {
                                    setTitle(getString(R.string.menu_text_profile));
                                    MyProfileFragment f_new = new MyProfileFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (4): {
                                    setTitle(getString(R.string.menu_text_about));
                                    AboutFragment f_new = new AboutFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (5): {
                                    setTitle(getString(R.string.menu_text_setting));
                                    SettingFragment f_new = new SettingFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (2): {
                                    setTitle(getString(R.string.menu_text_orders));
                                    OrdersFragment f_new = new OrdersFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (3): {
                                    setTitle(getString(R.string.menu_text_manual));
                                    ManualFragment f_new = new ManualFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                            }
                            return false;
                        }


                    })
                    .build();
        }
        else{
            result = new DrawerBuilder()
                    .withActivity(this)
                    .withToolbar(activityMainBinding.toolbar)
                    .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withIdentifier(6)
                                .withName(R.string.menu_text_new_order)
                                .withIcon(R.drawable.ic_plus)
                                .withIconTintingEnabled(true)
                                .withIconColor(ConstClass.getColor(getActivity(),R.color.base_label_color))
                                .withTextColor(ConstClass.getColor(getActivity(),R.color.base_label_color))
                                .withSelectedTextColor(ConstClass.getColor(getActivity(),R.color.base_value_color))
                                .withSelectedIconColor(ConstClass.getColor(getActivity(),R.color.base_value_color)),
                            new DividerDrawerItem(),
                            new PrimaryDrawerItem()
                                    .withIdentifier(2)
                                    .withName(R.string.menu_text_orders)
                                    .withIcon(R.drawable.ic_case)
                                    .withIconTintingEnabled(true)
                                    .withIconColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withTextColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withSelectedTextColor(ConstClass.getColor(getActivity(), R.color.base_value_color))
                                    .withSelectedIconColor(ConstClass.getColor(getActivity(), R.color.base_value_color)),

                            new PrimaryDrawerItem()
                                    .withIdentifier(3)
                                    .withName(R.string.menu_text_manual)
                                    .withIcon(R.drawable.ic_alert)
                                    .withIconTintingEnabled(true)
                                    .withIconColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withTextColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withSelectedTextColor(ConstClass.getColor(getActivity(), R.color.base_value_color))
                                    .withSelectedIconColor(ConstClass.getColor(getActivity(), R.color.base_value_color)),
                            new PrimaryDrawerItem()
                                    .withIdentifier(5)
                                    .withName(R.string.menu_text_setting)
                                    .withIcon(R.drawable.ic_setting)
                                    .withIconTintingEnabled(true)
                                    .withIconColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withTextColor(ConstClass.getColor(getActivity(), R.color.base_label_color))
                                    .withSelectedTextColor(ConstClass.getColor(getActivity(), R.color.base_value_color))
                                    .withSelectedIconColor(ConstClass.getColor(getActivity(), R.color.base_value_color))
                        /*new PrimaryDrawerItem()
                                .withIdentifier(4)
                                .withName(R.string.menu_text_about)
                                .withIcon(R.drawable.ic_information)
                                .withIconTintingEnabled(true)
                                .withIconColor(ConstClass.getColor(getActivity(),R.color.base_label_color))
                                .withTextColor(ConstClass.getColor(getActivity(),R.color.base_label_color))
                                .withSelectedTextColor(ConstClass.getColor(getActivity(),R.color.base_value_color))
                                .withSelectedIconColor(ConstClass.getColor(getActivity(),R.color.base_value_color))*/
                    )
                    .withActionBarDrawerToggle(true)
                    .withAccountHeader(headerResult)
                    .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                            switch ((int) drawerItem.getIdentifier()) {
                                case (1): {
                                    setTitle(getString(R.string.menu_text_profile));
                                    MyProfileFragment f_new = new MyProfileFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (4): {
                                    setTitle(getString(R.string.menu_text_about));
                                    AboutFragment f_new = new AboutFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (5): {
                                    setTitle(getString(R.string.menu_text_setting));
                                    SettingFragment f_new = new SettingFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (2): {
                                    setTitle(getString(R.string.menu_text_orders));
                                    OrdersFragment f_new = new OrdersFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (3): {
                                    setTitle(getString(R.string.menu_text_manual));
                                    ManualFragment f_new = new ManualFragment();
                                    goToFragment(f_new);
                                    break;
                                }
                                case (6): {
                                    Intent intent = new Intent(getActivity(), CreateOrder.class);
                                    startActivity(intent);
                                    break;
                                }
                            }
                            return false;
                        }


                    })
                    .build();
        }
        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(!result.isDrawerOpen())
                    result.openDrawer();
                else
                    result.closeDrawer();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isVisible = false;
    }

    public void ToastNotify(final String notificationMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(Auth.this, notificationMessage, Toast.LENGTH_LONG).show();
                //TextView helloText = (TextView) findViewById(R.id.text_hello);
                //Toast.setText(notificationMessage);
            }
        });
    }


    public static final int CHOOSE_RESULT = 1;
    private String selectedPass;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /**
     * Create a file Uri for saving an image or video
     */
    private static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public boolean checkString(String string) {
        if (string == null) return false;
        return string.matches("^-?\\d+$");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_PROFILE:
                if (resultCode == Activity.RESULT_OK) {
                    reload();
                }
                break;
            case REQUEST_EXIT:
                if (resultCode == Activity.RESULT_OK) {
                    reload();
                }
                break;
            case CHOOSE_RESULT:
                if (data != null) {
                    // showToast("Вы выбрали фотографию!");
                    Uri selectedImage = data.getData();
                    try {
                        Toast.makeText(getActivity(),"Загрзука аватарки началась!",Toast.LENGTH_LONG).show();
                    }catch (Exception e){

                    }
                    selectedPass = ConstClass.getRealPathFromURI(getApplicationContext(), selectedImage);
                    if(WebFunctionService.getTOKEN()!=null)
                        webFunctionService.uploadAva(selectedPass, new Callback<BaseStruct<User>>() {

                            @Override
                            public void success(BaseStruct<User> userBaseStruct, Response response) {
                                if(userBaseStruct.getStatus()==1) {
                                    CurrentUserProfile currentUserProfile = CurrentUserProfile.getInstance(getActivity());
                                    if (currentUserProfile.loginUser(userBaseStruct, response)!=null) {
                                        currentUserProfile.loadUser(userBaseStruct.getResult().get(0));
                                        appSetting.saveSharedPreferences(AppSetting.SETTING_MY_PROFILE_ID,userBaseStruct.getResult().get(0).getId());
                                        getActivity().reload();
                                    }
                                    else{
                                        Toast.makeText(getApplicationContext(),userBaseStruct.getMessage(),Toast.LENGTH_LONG).show();
                                    }
                                }
                                else {
                                    Toast.makeText(getApplicationContext(),userBaseStruct.getMessage(),Toast.LENGTH_LONG).show();
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {
                            }
                        });
                }

                break;
        }
    }
}
