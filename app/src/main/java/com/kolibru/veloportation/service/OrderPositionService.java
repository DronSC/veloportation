package com.kolibru.veloportation.service;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Binder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.kolibru.veloportation.ConstClass;
import com.kolibru.veloportation.R;
import com.kolibru.veloportation.activities.FreeMap;
import com.kolibru.veloportation.models.BaseStruct;
import com.kolibru.veloportation.models.OrderPosition;
import com.kolibru.veloportation.models.UserPosition;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

import nl.changer.audiowife.AudioWife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class OrderPositionService extends BaseNavigationService {

    public static final String NEW_LOCATION_INFO = "new_location_info";

    public static final int NOTIFICATION_ID = 20160129;

    private NotificationCompat.Builder mNotification;
    private NotificationManager mNotifyMgr;
    static LocationListener myLocationListener;


    @Override
    public void onCreate() {
        super.onCreate();
        mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        myLocationListener = new MyLocationListener();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return START_STICKY;
        }

        changeLocationRequest(time);

        //initAndShowNotification();
        return START_STICKY;
    }

    private void changeLocationRequest(long time) {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            if (locationManager.isProviderEnabled(provider))
                locationManager.removeUpdates(myLocationListener);
            locationManager.requestLocationUpdates(provider, time, 2, myLocationListener);
        } catch (Exception e) {

        }
    }

    /*private void initAndShowNotification() {
        Intent intent = new Intent(getApplicationContext(), FreeMap.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent piAct = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        mNotification = new NotificationCompat.Builder(this);
        mNotification
                .setContentTitle(getString(R.string.text_record_tracker))
                .setSmallIcon(R.drawable.bicycle)
                .setAutoCancel(true)
                .setColor(ConstClass.getColor(getApplicationContext(), R.color.primary))
                .setContentIntent(piAct);
        mNotifyMgr.notify(NOTIFICATION_ID, mNotification.build());
    }


    private void updateNotificationInfo() {
        try {
            Intent intent = new Intent(ROUTE_STATE_INFO);
            sendBroadcast(intent);

            mNotification
                    .setContentTitle(getString(R.string.text_record_tracker))
                    .setContentText(getString(R.string.text_in_track)+String.valueOf(routeState.getDistance())+" м");

            mNotifyMgr.notify(NOTIFICATION_ID, mNotification.build());
        } catch (Exception e) {

        }
    }*/


    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(final Location location) {


            if (mTimeLast == null) {
                mTimeLast = DateTime.now();
            }

            Intent intent = new Intent(NEW_LOCATION_INFO);
            intent.putExtra(LOCATION_SERVICE, location);
            sendBroadcast(intent);

            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            if ((Seconds.secondsBetween(mTimeLast, DateTime.now()).getSeconds() > 5)) {
                mTimeLast = DateTime.now();
                webFunctionService.saveOrderPosition(latLng.latitude, latLng.longitude, new Callback<BaseStruct<UserPosition>>() {
                    @Override
                    public void success(BaseStruct<UserPosition> userPositionBaseStruct, Response response) {
                        Log.e("setMyPosition", String.valueOf(userPositionBaseStruct.getResult().size()));

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("setMyPosition", "ERROR:" + ConstClass.responseToString(error.getResponse()));
                    }
                });
            }

            // updateNotificationInfo();

        }

        @Override
        public void onProviderDisabled(final String provider) {
        }

        @Override
        public void onProviderEnabled(final String provider) {
        }

        @Override
        public void onStatusChanged(final String provider, final int status, final Bundle extras) {
        }

    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.removeUpdates(myLocationListener);

        } catch (Exception e) {

        }

        mNotifyMgr.cancel(NOTIFICATION_ID);
    }

}
