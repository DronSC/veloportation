package com.kolibru.veloportation.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;


import com.kolibru.veloportation.web.WebFunctionService;

import org.joda.time.DateTime;


public class BaseNavigationService extends Service {

    public static final String ROUTE_STATE_INFO = "route_state_info";
    public static final String ROUTE = "route";
    public static final String NEW_LOCATION_INFO = "new_location_info";

    protected int time = 1000;
    protected Binder mBinder;
    protected DateTime mTimeLast=null;

    protected LocationManager locationManager;
    protected String provider;
    protected WebFunctionService webFunctionService=null;

    protected float getDistance(Location location, Location nowLocation) {
        float[] results = new float[1];
        Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                nowLocation.getLatitude(), nowLocation.getLongitude(), results);
        return results[0];
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        webFunctionService= WebFunctionService.getInstance(getApplicationContext());
        mBinder = new PlaceBinder();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        provider = locationManager.getBestProvider(criteria, false);
        //Log.e("LAST POSITION", provider);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }



    /*private void displayNotificationImage(String image) {
        Glide.with(getApplicationContext())
                .load(ConstClass.getPictureLink(image, ConstClass.getWidth()))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        mNotification.setLargeIcon(resource);
                    }

                    @Override
                    public void onLoadFailed(Exception resource, Drawable drawable) {
                        try {
                            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.image_load);
                            mNotification.setLargeIcon(bm);
                        } catch (Exception e) {
                        }
                    }
                });
    }

    private void updateNotificationInfo(Point point, String subtitle) {
        try {
            Intent intent = new Intent(ROUTE_STATE_INFO);
            sendBroadcast(intent);

            mNotification.setContentTitle(point.getTitle())
                    .setContentText(subtitle);

            mNotifyMgr.notify(NOTIFICATION_ID, mNotification.build());
            if (point.getImage().size() > 0)
                displayNotificationImage(point.getImage().get(0).getName());
        } catch (Exception e) {

        }
    }*/



    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public class PlaceBinder extends Binder {

        public BaseNavigationService getService() {
            return BaseNavigationService.this;
        }

    }
}
